#! /bin/bash

cp ../../toml/myfilter.toml ../eventhub/;
sleep 1;
cd ../eventhub/;
./event-hub -config myfilter.toml> ../result/hekaoutput.txt &
sleep 1;
cd ../../aggregators/
node reportgenerator.js> ../test/result/reportgeneratoroutput.txt &
node deltatimestamp.js> ../test/result/deltatimestampoutput.txt &
node savedb.js> ../test/result/savedboutput.txt &
sleep 1;
for ((i=1;i<=10;i++)); do curl -d '{"component":"ossec","object":"os","labels":["sla_id_ardeefce","security_mechanism_specs-monitoring-ossec","alert"],"type":"alert","data":"{generatedAlerts:report number}","timestamp":950}' http://localhost:8080/events/prova; done

for ((i=1;i<=10;i++)); do curl -d '{"component":"ossec","object":"os","labels":["sla_id_ardeefce","security_mechanism_specs-monitoring-ossec","alert"],"type":"alert","data":"{generatedAlerts:report number}","timestamp":1950}' http://localhost:8080/events/prova; done

curl -d '{"component":"openvas","object":"vulnerability_list","labels":["sla_id_ardeefce","security_mechanism_specs-monitoring-openvas","report"],"type":"alert","data":"{generatedAlerts:report number}","timestamp":1950}' http://localhost:8080/events/prova

curl -d '{"component":"openvas","object":"vulnerability_list","labels":["sla_id_ardeefce","security_mechanism_specs-monitoring-openvas","report"],"type":"alert","data":"{generatedAlerts:report number}","timestamp":2950}' http://localhost:8080/events/prova

curl -d '{"component":"openvas","object":"lalaalala","labels":["sla_id_ardeefce","security_mechanism_specs-monitoring-openvas","report"],"type":"alert","data":"{generatedAlerts:report number}","timestamp":3950}' http://localhost:8080/events/prova

curl -d '{"component":"openvas","object":"lalaalala","labels":["sla_id_ardeefce","security_mechanism_specs-monitoring-openvas","report"],"type":"alert","data":"{generatedAlerts:report number}","timestamp":5950}' http://localhost:8080/events/prova
sleep 1;
pkill event-hub;
