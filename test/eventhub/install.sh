#!/bin/bash

echo -n > install.log

if [ -z "$GOROOT" ]
then echo "GOROOT not set in your system. Make sure you have Go installed and GOROOT set." 2>&1 | tee --append install.log; exit 1
fi

if [ -z "$GOPATH" ]
then echo "GOPATH not set in your system. Make sure you have GOPATH set." 2>&1 | tee --append install.log; exit 1
fi

GO_VERSION=`go version`
GO_VERSION=`echo "$GO_VERSION" | sed -e "s/^[^0-9]*\([0-9.]\+\).*$/\1/"`
echo "Found Go version $GO_VERSION" 2>&1 | tee --append install.log

EXPECTED_GO_VERSION="1.3"

compareVersions ()
{
  typeset    IFS='.'
  typeset -a v1=( $GO_VERSION )
  typeset -a v2=( $EXPECTED_GO_VERSION )
  typeset    n diff

  for (( n=0; n<4; n+=1 )); do
    diff=$((v1[n]-v2[n]))
    if [ $diff -ne 0 ] ; then
      [ $diff -le 0 ] && COMP_RES=-1 || COMP_RES=1
      return
    fi
  done
  COMP_RES=0
}

compareVersions
if [ -1 -eq $COMP_RES ]
then echo "You need Go >=1.3 to run the tool." 2>&1 | tee --append install.log; exit 1
fi

buildHeka() 
{
	CDIR=`pwd`
	GOROOT_BCK=$GOROOT
	GOPATH_BCK=$GOPATH
	#HEKA_VERSION=o
	echo "Building Heka..." 2>&1 | tee --append $CDIR/install.log
	cd /tmp/heka
	source ./build.sh 2>&1 | tee --append $CDIR/install.log
	if [ 0 -ne ${PIPESTATUS[0]} ]
	then echo "Building Heka failed." 2>&1 | tee --append $CDIR/install.log;cd $CDIR; exit 1
	else echo "Heka built successfully." 2>&1 | tee --append $CDIR/install.log
	fi
	export GOROOT=$GOROOT_BCK
	export GOPATH=$GOPATH_BCK
	cd /tmp/heka/build/heka/pkg
	if [ 0 -ne $? ]
	then echo "Failed to cd." 2>&1 | tee --append $CDIR/install.log; cd $CDIR; exit 1
	fi
	if [ ! -e $GOPATH/pkg ]
	then mkdir $GOPATH/pkg
	fi
	cp -R ./* $GOPATH/pkg
	if [ 0 -ne $? ]
	then echo "Failed to copy Heka pkg." 2>&1 | tee --append $CDIR/install.log; cd $CDIR; exit 1
	fi
	cd /tmp/heka/build/heka/bin
	if [ 0 -ne $? ]
	then echo "Failed to cd." 2>&1 | tee --append $CDIR/install.log; cd $CDIR; exit 1
	fi
	if [ ! -e $GOPATH/bin ]
	then mkdir $GOPATH/bin
	fi
	cp -R ./* $GOPATH/bin
	if [ 0 -ne $? ]
	then echo "Failed to copy Heka bin." 2>&1 | tee --append $CDIR/install.log; cd $CDIR; exit 1
	fi

	rm -fR $GOPATH/src/github.com/mozilla-services/heka
	if [ 0 -ne $? ]
	then echo "Failed to remove Heka." 2>&1 | tee --append $CDIR/install.log; cd $CDIR; exit 1
	fi
	cd /tmp/heka/build/heka/src
	if [ 0 -ne $? ]
	then echo "Failed to cd." 2>&1 | tee --append $CDIR/install.log; cd $CDIR; exit 1
	fi
	rsync -av --exclude=".*" ./* $GOPATH/src
	if [ 0 -ne $? ]
	then echo "Failed to copy Heka src from /tmp." 2>&1 | tee --append $CDIR/install.log; cd $CDIR; exit 1
	fi
	rm -fR /tmp/heka
	if [ 0 -ne $? ]
	then echo "Failed to remove /tmp/heka." 2>&1 | tee --append $CDIR/install.log; cd $CDIR; exit 1
	fi
	cd $CDIR
}


echo "Getting Heka..." 2>&1 | tee --append install.log
git clone https://github.com/mozilla-services/heka /tmp/heka 2>&1 | tee --append install.log
if [ 0 -ne ${PIPESTATUS[0]} ]
then echo "Getting Heka failed." 2>&1 | tee --append install.log; exit 1
else echo "Heka downloaded successfully." 2>&1 | tee --append install.log
fi

buildHeka

go get -d bitbucket.org/georgiana_m/specs-monitoring/event-hub 2>&1 | tee --append install.log
if [ 0 -ne ${PIPESTATUS[0]} ]
then echo "Getting packages failed." 2>&1 | tee --append install.log; exit 1
else echo "Packages downloaded successfully." 2>&1 | tee --append install.log
fi

go build bitbucket.org/georgiana_m/specs-monitoring/event-hub 2>&1 | tee --append install.log
if [ 0 -ne ${PIPESTATUS[0]} ]
then echo "Build failed." 2>&1 | tee --append install.log; exit 1
else echo "Build done." 2>&1 | tee --append install.log
fi

