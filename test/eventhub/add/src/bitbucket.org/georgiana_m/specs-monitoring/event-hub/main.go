package main

import (
	_ "bitbucket.org/georgiana_m/specs-monitoring/aggregators"
	"bitbucket.org/georgiana_m/specs-monitoring/core"
	"flag"
	"fmt"
	"github.com/mozilla-services/heka/message"
	"github.com/mozilla-services/heka/pipeline"
	"io/ioutil"
	"os"
	"path/filepath"
	"runtime"
	"runtime/pprof"
	"strconv"
	"strings"
	"syscall"
)

const (
	routerConfigFileName = "./specs_router.toml"
	VERSION              = "0.2"
)

func main() {
	configPath := flag.String("config", "<file|directory>",
		"Config file or directory. If directory is specified then all files "+
			"in the directory will be loaded.")
	version := flag.Bool("version", false, "Output version and exit")
	flag.Parse()

	if *version {
		fmt.Println(VERSION)
		os.Exit(0)
	}

	if "<file|directory>" == *configPath && false == *version {
		fmt.Fprintf(os.Stderr, "Usage: %s <option>. Possible options:\n", os.Args[0])
		flag.PrintDefaults()
		os.Exit(0)
	}

	config := &HekadConfig{}
	var err error
	var cpuProfName string
	var memProfName string

	config, err = LoadHekadConfig(*configPath)
	if err != nil {
		pipeline.LogError.Fatal("Error reading config: ", err)
	}
	if config.SampleDenominator <= 0 {
		pipeline.LogError.Fatalln("'sample_denominator' value must be greater than 0.")
	}
	globals, cpuProfName, memProfName := setGlobalConfigs(config)

	if err = os.MkdirAll(globals.BaseDir, 0755); err != nil {
		pipeline.LogError.Fatalf("Error creating 'base_dir' %s: %s", config.BaseDir, err)
	}

	if config.MaxMessageSize > 1024 {
		message.SetMaxMessageSize(config.MaxMessageSize)
	} else if config.MaxMessageSize > 0 {
		pipeline.LogError.Fatalln("Error: 'max_message_size' setting must be greater than 1024.")
	}
	if config.PidFile != "" {
		contents, err := ioutil.ReadFile(config.PidFile)
		if err == nil {
			pid, err := strconv.Atoi(strings.TrimSpace(string(contents)))
			if err != nil {
				pipeline.LogError.Fatalf("Error reading proccess id from pidfile '%s': %s",
					config.PidFile, err)
			}

			process, err := os.FindProcess(pid)

			// on Windows, err != nil if the process cannot be found
			if runtime.GOOS == "windows" {
				if err == nil {
					pipeline.LogError.Fatalf("Process %d is already running.", pid)
				}
			} else if process != nil {
				// err is always nil on POSIX, so we have to send the process
				// a signal to check whether it exists
				if err = process.Signal(syscall.Signal(0)); err == nil {
					pipeline.LogError.Fatalf("Process %d is already running.", pid)
				}
			}
		}
		if err = ioutil.WriteFile(config.PidFile, []byte(strconv.Itoa(os.Getpid())),
			0644); err != nil {

			pipeline.LogError.Fatalf("Unable to write pidfile '%s': %s", config.PidFile, err)
		}
		pipeline.LogInfo.Printf("Wrote pid to pidfile '%s'", config.PidFile)
		defer func() {
			if err = os.Remove(config.PidFile); err != nil {
				pipeline.LogError.Printf("Unable to remove pidfile '%s': %s", config.PidFile, err)
			}
		}()
	}

	if cpuProfName != "" {
		profFile, err := os.Create(cpuProfName)
		if err != nil {
			pipeline.LogError.Fatalln(err)
		}

		pprof.StartCPUProfile(profFile)
		defer func() {
			pprof.StopCPUProfile()
			profFile.Close()
		}()
	}

	if memProfName != "" {
		defer func() {
			profFile, err := os.Create(memProfName)
			if err != nil {
				pipeline.LogError.Fatalln(err)
			}
			pprof.WriteHeapProfile(profFile)
			profFile.Close()
		}()
	}

	routerRequestHandlers, err := core.DecodeRouterHandlerConfiguration(config.HandlersConfiguration)
	if nil != err {
		pipeline.LogError.Fatal("Error reading router request handlers: ", err)
	}

	// Set up and load the pipeline configuration and start the daemon.
	pipeconf := pipeline.NewPipelineConfig(globals)
	if err = loadFullConfig(pipeconf, configPath, routerRequestHandlers, config.HandlersConfiguration); err != nil {
		pipeline.LogError.Fatal("Error reading config: ", err)
	}

	// create stream buffers
	core.RouterBuffer = core.NewRouterStreamBuffers(config.BufferSize)

	go core.HandleHttpRequests(routerRequestHandlers, pipeconf, config.Port)

	pipeline.Run(pipeconf)
}

func setGlobalConfigs(config *HekadConfig) (*pipeline.GlobalConfigStruct, string, string) {
	maxprocs := config.Maxprocs
	poolSize := config.PoolSize
	chanSize := config.ChanSize
	cpuProfName := config.CpuProfName
	memProfName := config.MemProfName
	maxMsgLoops := config.MaxMsgLoops
	maxMsgProcessInject := config.MaxMsgProcessInject
	maxMsgProcessDuration := config.MaxMsgProcessDuration
	maxMsgTimerInject := config.MaxMsgTimerInject

	runtime.GOMAXPROCS(maxprocs)

	globals := pipeline.DefaultGlobals()
	globals.PoolSize = poolSize
	globals.PluginChanSize = chanSize
	globals.MaxMsgLoops = maxMsgLoops
	if globals.MaxMsgLoops == 0 {
		globals.MaxMsgLoops = 1
	}
	globals.MaxMsgProcessInject = maxMsgProcessInject
	globals.MaxMsgProcessDuration = maxMsgProcessDuration
	globals.MaxMsgTimerInject = maxMsgTimerInject
	globals.BaseDir = config.BaseDir
	globals.ShareDir = config.ShareDir
	globals.SampleDenominator = config.SampleDenominator
	globals.Hostname = config.Hostname

	return globals, cpuProfName, memProfName
}

func loadFullConfig(pipeconf *pipeline.PipelineConfig, configPath *string, requestHandlers []core.RouterRequestHandlerConfig, base64Handlers string) (err error) {
	p, err := os.Open(*configPath)
	if err != nil {
		return fmt.Errorf("error opening file: %s", err.Error())
	}
	fi, err := p.Stat()
	if err != nil {
		return fmt.Errorf("can't stat file: %s", err.Error())
	}

	if fi.IsDir() {
		files, _ := ioutil.ReadDir(*configPath)
		for _, f := range files {
			fName := f.Name()
			if !strings.HasSuffix(fName, ".toml") {
				// Skip non *.toml files in a config dir.
				continue
			}
			err = pipeconf.PreloadFromConfigFile(filepath.Join(*configPath, fName))
			if err != nil {
				break
			}
		}
	} else {
		err = pipeconf.PreloadFromConfigFile(*configPath)
	}

	if nil != err {
		return err
	}

	// load router extra config
	extendedConfig := make(map[string]interface{})

	for _, handler := range requestHandlers {
		if "" != handler.Decoder {
			_, ok := extendedConfig[handler.Decoder]
			if !ok {
				elemMap := make(map[string]interface{})
				elemMap["type"] = handler.Decoder
				extendedConfig[handler.Decoder] = elemMap
			}
		}
	}

	// add router input decoder
	decoderMap := make(map[string]interface{})
	decoderMap["type"] = "RouterMultiDecoder"
	decoderMap["configuration"] = base64Handlers
	extendedConfig[core.ROUTER_DECODER_NAME] = decoderMap

	// add router input
	inputMap := make(map[string]interface{})
	inputMap["type"] = "EventRouterInput"
	inputMap["configuration"] = base64Handlers
	inputMap["decoder"] = core.ROUTER_DECODER_NAME
	extendedConfig[core.ROUTER_INPUT_NAME] = inputMap

	// add specs decoder and encoder (if not already there)
	_, ok := extendedConfig["SpecsDecoder"]
	if !ok {
		elemMap := make(map[string]interface{})
		elemMap["type"] = "SpecsDecoder"
		extendedConfig["SpecsDecoder"] = elemMap
	}

	// add router encoder
	encoderMap := make(map[string]interface{})
	encoderMap["type"] = "RouterEncoder"
	extendedConfig[core.ROUTER_ENCODER_NAME] = encoderMap

	// add router output
	outputMap := make(map[string]interface{})
	outputMap["type"] = "RouterOutput"
	outputMap["encoder"] = core.ROUTER_ENCODER_NAME
	extendedConfig[core.ROUTER_OUTPUT_NAME] = outputMap

	// write extra configuration to a file
	err = core.WriteTomlFile(routerConfigFileName, extendedConfig)
	if err != nil {
		return
	}

	err = pipeconf.PreloadFromConfigFile(routerConfigFileName)

	if err == nil {
		err = pipeconf.LoadConfig()
	}
	return err
}
