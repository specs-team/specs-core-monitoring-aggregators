package core

import (
	"encoding/json"
	"fmt"
)

type EventEncoder interface {
	Encode(event SpecsStreamedEvent) (output []byte, err error)
}

// SpecsEventEncoder encodes a SPECS streamed event
// into a JSON representation of its embedded SPECS Monitoring Message..
type SpecsEventEncoder struct {
}

func CreateSpecsEventEncoder() *SpecsEventEncoder {
	return new(SpecsEventEncoder)
}

// Encode encodes the SPECS streamed event
// into a JSON representation of its embedded SPECS Monitoring Message.
func (se *SpecsEventEncoder) Encode(event SpecsStreamedEvent) (output []byte, err error) {
	mssg := event.MonitoringMessage

	payload, merr := json.Marshal(mssg)
	if merr != nil {
		err = fmt.Errorf("Could not serialiaze JSON %+v. Error: %s", mssg, merr)
		return
	}
	// Just payload
	output = []byte(payload)

	return
}
