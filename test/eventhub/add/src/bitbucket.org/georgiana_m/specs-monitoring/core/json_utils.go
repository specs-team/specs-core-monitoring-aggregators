package core

import (
	"encoding/json"
	"fmt"
	"reflect"
)

func UnmarshalJsonString(input string) (data interface{}, err error) {
	if "" == input {
		return
	}
	err = json.Unmarshal([]byte(input), &data)
	return
}

func UnmarshalJsonBytes(jsonBlob []byte) (data interface{}, err error) {
	err = json.Unmarshal(jsonBlob, &data)
	return
}

func UnmarshalJsonBytesToStringArray(jsonBlob []byte) (arr []string, err error) {
	err = json.Unmarshal(jsonBlob, &arr)
	return
}

func MarshalData(data interface{}) (bytes []byte, err error) {
	bytes, err = json.Marshal(data)
	return
}

// Unmarshalls a JSON string (its byte representation) into a struct.
// The elem parameter is actually the address of the struct element where the
// result of the unmarshalling process will be hold.
func UnmarshalJsonToStruct(jsonBlob []byte, elem interface{}) (err error) {
	var mapData map[string]interface{}

	err = json.Unmarshal(jsonBlob, &mapData)
	if nil != err {
		return
	}

	err = unmarshalJsonToStruct(&mapData, elem)
	return
}

// Unmarshalls a JSON string (its byte representation) into a struct.
// The elem parameter is actually the address of the struct element where the
// result of the unmarshalling process will be hold.
func DecoderUnmarshalJsonToStruct(dec *json.Decoder, elem interface{}) (err error) {
	var mapData map[string]interface{}

	err = dec.Decode(&mapData)
	if nil != err {
		return
	}

	err = unmarshalJsonToStruct(&mapData, elem)
	return
}

func unmarshalJsonToStruct(mapDataPtr *map[string]interface{}, elem interface{}) (err error) {
	var mapData map[string]interface{}
	mapData = *mapDataPtr
	var (
		elemValue reflect.Value
		elemType  reflect.Type
	)

	if reflect.TypeOf(elem).Kind() == reflect.Ptr {
		elemValue = reflect.ValueOf(elem).Elem()
		elemType = reflect.TypeOf(elem).Elem()
	} else {
		elemValue = reflect.ValueOf(elem)
		elemType = reflect.TypeOf(elem)
	}

	for i := 0; i < elemType.NumField(); i++ {
		field := elemType.Field(i)
		fieldInstance := elemValue.Field(i)
		assignedVal := mapData[field.Name]
		if nil == assignedVal {
			assignedVal = mapData[field.Tag.Get("json")]
		}
		if nil == assignedVal {
			continue
		}
		if fieldInstance.Kind() == reflect.Struct {
			pointer := reflect.New(field.Type).Interface()
			mapVal, ok := assignedVal.(map[string]interface{})
			if !ok {
				err = fmt.Errorf("[unmarshalJson] Value %s: %v is not of type map[string]interface{}.", field.Name, assignedVal)
				return
			}

			err = ConvertMapToStruct(mapVal, pointer)
			if nil != err {
				return
			}
			convertedVal := reflect.Indirect(reflect.ValueOf(pointer)).Convert(fieldInstance.Type())
			fieldInstance.Set(convertedVal)
		} else if fieldInstance.Kind() == reflect.Slice || fieldInstance.Kind() == reflect.Array {
			iAssignedVal, ok := assignedVal.([]interface{})
			if !ok {
				err = fmt.Errorf("[unmarshalJson] Value %s: %v is not of type []interface{}.", field.Name, assignedVal)
				return
			}

			cAssignedVal := reflect.MakeSlice(fieldInstance.Type(), 0, 0).Interface()
			result := ConvertToTypeSlice(cAssignedVal, &iAssignedVal, fieldInstance.Type())
			convertedVal := reflect.ValueOf(result).Convert(fieldInstance.Type())
			fieldInstance.Set(convertedVal)
		} else {
			convertedVal := reflect.ValueOf(assignedVal).Convert(fieldInstance.Type())
			fieldInstance.Set(convertedVal)
		}
	}

	return
}
