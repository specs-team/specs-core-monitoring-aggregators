package core

import (
	"code.google.com/p/go-uuid/uuid"
	"fmt"
	"github.com/mozilla-services/heka/pipeline"
	"io/ioutil"
	"log"
	"mime/multipart"
	"net/http"
	"net/url"
	"regexp"
	"strconv"
	"strings"
	"time"
)

const (
	POST_EVENTS_PATTERN = "^/events/([^/]+)$"
	GET_EVENTS_PATTERN  = "^/streams/([^/]+)/consume$"
	EVENT_SEPARATOR     = "\r"
)

type RegexpHandler struct {
	requestHandlers []*RouterRequestHandlerConfig
	eventChan       chan http.Request
	pConfig         *pipeline.PipelineConfig
}

type HttpRequest struct {
	Method           string
	URL              *url.URL
	Proto            string // "HTTP/1.0"
	ProtoMajor       int    // 1
	ProtoMinor       int    // 0
	Header           http.Header
	ContentLength    int64
	TransferEncoding []string
	Host             string
	Form             url.Values
	PostForm         url.Values
	MultipartForm    *multipart.Form
	Trailer          http.Header
	RemoteAddr       string
	RequestURI       string
	Body             []byte
}

func (h *RegexpHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	for _, rHandler := range h.requestHandlers {
		if rHandler.pattern.MatchString(r.URL.Path) {
			rHandler.handler.ServeHTTP(w, r)
			return
		}
	}
	// no pattern matched; send 404 response
	http.NotFound(w, r)
}

func (h *RegexpHandler) SendEvents(w http.ResponseWriter, r *http.Request) {
	if "GET" == r.Method {
		rrConfig := h.getRequestRouterConfig(r.URL.Path)
		if nil == rrConfig {
			err := fmt.Errorf("No handler found for path %s.", r.URL.Path)
			pipeline.LogError.Println(err)
			http.Error(w, fmt.Sprintf("No handler found for path %s.", r.URL.Path), 400)
			return
		}
		matches := rrConfig.pattern.FindStringSubmatch(r.URL.Path)
		if len(matches) == 0 {
			err := fmt.Errorf("Invalid URI path: %s.", r.URL.Path)
			pipeline.LogError.Println(err)
			http.Error(w, fmt.Sprintf("Invalid URI path: %s.", r.URL.Path), 400)
			return
		}
		if 0 == rrConfig.StreamGroup {
			err := fmt.Errorf("Invalid handler for URI path: %s. No path_pattern_stream_group defined in handler.", r.URL.Path)
			pipeline.LogError.Println(err)
			http.Error(w, fmt.Sprintf("Invalid handler for URI path: %s. No path_pattern_stream_group defined in handler.", r.URL.Path), 400)
			return
		}
		stream := matches[rrConfig.StreamGroup]

		streamToken := RouterBuffer.GetToken(stream)
		var receivedToken *MessageToken
		var lastToken *MessageToken
		if marshalledReceivedToken := r.Header.Get(SPECS_TOKEN_HEADER_NAME); "" != marshalledReceivedToken {
			tokenElems := strings.Split(marshalledReceivedToken, ":")
			tokenUUID := uuid.Parse(tokenElems[0])
			seq, err := strconv.ParseInt(tokenElems[1], 10, 64)
			if nil == err {
				receivedToken = new(MessageToken)
				receivedToken.UUID = tokenUUID
				receivedToken.SequenceNo = seq
				lastToken = receivedToken
				if !uuid.Equal(tokenUUID, streamToken.UUID) {
					lastToken = nil
				}
			} else {
				lastToken = nil
			}
		}

		encoder, err := h.getEncoder(rrConfig.Encoder)
		if nil != err {
			pipeline.LogError.Println(err)
			http.Error(w, fmt.Sprintf("No encoder found for the handler: %s.", rrConfig.Encoder), 400)
			return
		}

		pipeline.LogInfo.Printf("Sending events for stream %s to %s...", stream, r.RemoteAddr)

		notify := w.(http.CloseNotifier).CloseNotify()
		stop := false
		for !stop {
			select {
			case <-notify:
				pipeline.LogInfo.Printf("Client %s closed connection.", r.RemoteAddr)
				stop = true
				break
			default:
				outData := RouterBuffer.Values(stream, lastToken)
				//pipeline.LogInfo.Printf("[%s] Last token: %+v (%d events).", r.RemoteAddr, lastToken, len(outData))
				for _, event := range outData {
					lastToken = event.MonitoringMessage.Token.Copy()
					outBytes, err := encoder.Encode(*event)
					if nil != err {
						pipeline.LogError.Println(err)
						http.Error(w, fmt.Sprintf("%s.", err), 400)
						continue
					}
					sentToken := fmt.Sprintf("%s:%d", event.MonitoringMessage.Token.UUID.String(), event.MonitoringMessage.Token.SequenceNo)
					//pipeline.LogInfo.Printf("Will send [%s] %s", sentToken, outBytes)
					w.Header().Set(SPECS_TOKEN_HEADER_NAME, sentToken)
					fmt.Fprintf(w, fmt.Sprintf("%s", outBytes))
					fmt.Fprintf(w, fmt.Sprintf(EVENT_SEPARATOR))
					if f, ok := w.(http.Flusher); ok {
						f.Flush()
					}
				}
				time.Sleep(100 * time.Millisecond)
			}
		}
	} else {
		http.Error(w, fmt.Sprintf("Expecting method %s.", r.Method), 405)
	}
}

func (h *RegexpHandler) ReceiveEvent(w http.ResponseWriter, r *http.Request) {
	channel, err := h.getEventChan()
	if nil == err {
		req := createHttpRequest(r)
		channel <- req
	} else {
		pipeline.LogError.Println(err)
	}
	w.WriteHeader(200)
}

func HandleHttpRequests(routerRequestHandlers []RouterRequestHandlerConfig, pConfig *pipeline.PipelineConfig, port int) {
	regexpHandler := new(RegexpHandler)

	for index, rrConfig := range routerRequestHandlers {
		rrConfig.pattern = regexp.MustCompile(rrConfig.PathPattern)
		routerRequestHandlers[index].pattern = rrConfig.pattern
		if ROUTER_INPUT_ACTION == rrConfig.Action {
			routerRequestHandlers[index].handler = func(w http.ResponseWriter, r *http.Request) {
				regexpHandler.ReceiveEvent(w, r)
			}
		} else if ROUTER_OUTPUT_ACTION == rrConfig.Action {
			routerRequestHandlers[index].handler = func(w http.ResponseWriter, r *http.Request) {
				regexpHandler.SendEvents(w, r)
			}
		}
		regexpHandler.requestHandlers = append(regexpHandler.requestHandlers, &routerRequestHandlers[index])
	}
	regexpHandler.pConfig = pConfig

	endpoint := fmt.Sprintf(":%d", port)
	log.Fatal(http.ListenAndServe(endpoint, regexpHandler))

	return
}

func (h *RegexpHandler) getEventChan() (channel chan *HttpRequest, err error) {
	inputRunner, ok := h.pConfig.InputRunners[ROUTER_INPUT_NAME]
	if !ok {
		err = fmt.Errorf("Cannot find router input.")
		return
	}
	input := inputRunner.Input()
	routerInput, ok := input.(*EventRouter)
	if !ok {
		err = fmt.Errorf("Cannot find router input: wrong input type.")
		return
	}
	channel = routerInput.InChan()
	return
}

func (h *RegexpHandler) getEncoder(encoderName string) (encoder EventEncoder, err error) {
	if "" == encoderName {
		err = fmt.Errorf("No encoder specified.")
		return
	}

	switch encoderName {
	case "SpecsEventEncoder":
		encoder = CreateSpecsEventEncoder()
		return
	}

	return
}

func createHttpRequest(src *http.Request) (req *HttpRequest) {
	//reqBytes, err := httputil.DumpRequestOut(src, true)
	req = new(HttpRequest)
	req.ContentLength = src.ContentLength
	req.Form = src.Form
	req.Header = src.Header
	req.Host = src.Host
	req.Method = src.Method
	req.MultipartForm = src.MultipartForm
	req.PostForm = src.PostForm
	req.Proto = src.Proto
	req.ProtoMajor = src.ProtoMajor
	req.ProtoMinor = src.ProtoMinor
	req.RemoteAddr = src.RemoteAddr
	req.RequestURI = src.RequestURI
	req.Trailer = src.Trailer
	req.TransferEncoding = src.TransferEncoding
	req.URL = src.URL
	req.Body, _ = ioutil.ReadAll(src.Body)
	return
}

func (h *RegexpHandler) getRequestRouterConfig(path string) (handler *RouterRequestHandlerConfig) {
	for _, rHandler := range h.requestHandlers {
		if rHandler.pattern.MatchString(path) {
			handler = rHandler
			return
		}
	}
	return
}
