// Package core contains core inputs, decoders, filters, encoders and outputs
// used in the SPECS event hub.
package core

import (
	"code.google.com/p/go-uuid/uuid"
	"github.com/mozilla-services/heka/message"
	"github.com/mozilla-services/heka/pipeline"
)

const (
	COMPONENT_FIELD_NAME   = "component"
	OBJECT_FIELD_NAME      = "object"
	LABELS_FIELD_NAME      = "labels"
	TYPE_FIELD_NAME        = "type"
	DATA_FIELD_NAME        = "data"
	TIMESTAMP_FIELD_NAME   = "stimestamp"
	TOKEN_FIELD_NAME       = "token"
	STREAM_FIELD_NAME      = "stream"
	ANNOTATIONS_FIELD_NAME = "annotations"

	HEKA_EVENT_MESSAGE_TYPE             = "specs.monitoring.event"
	HEKA_REGISTER_CLIENT_MESSAGE_TYPE   = "specs.monitoring.register.client"
	HEKA_UNREGISTER_CLIENT_MESSAGE_TYPE = "specs.monitoring.unregister.client"

	ANNOTATIONS_HEADER_NAME = "SPECS-Annotations"
	SPECS_TOKEN_HEADER_NAME = "SPECS-Heka-HTTP-Gateway-Stream-Sequence"
)

type MessageToken struct {
	UUID       uuid.UUID `json:"uuid"`
	SequenceNo int64     `json:"seq"`
}

// SpecsMonitoringMessage defines the SPECS input message structure.
type SpecsMonitoringMessage struct {
	Component string       `json:"component"`
	Object    string       `json:"object"`
	Labels    []string     `json:"labels"`
	Type      string       `json:"type"`
	Data      interface{}  `json:"data"`
	Timestamp float64      `json:"timestamp"`
	Token     MessageToken `json:"token"`
}

type SpecsStreamedEvent struct {
	MonitoringMessage SpecsMonitoringMessage
	Annotations       map[string]interface{}
	Stream            string
}

// AddFields adds custom fields to the Heka message, based on a SPECS input message.
// The resulted Heka message will be the SPECS internal message.
func AddFields(pack *pipeline.PipelinePack, mssg *SpecsMonitoringMessage) (err error) {
	var nf *message.Field
	nf = message.NewFieldInit(COMPONENT_FIELD_NAME, message.Field_STRING, "")
	nf.AddValue(mssg.Component)
	pack.Message.AddField(nf)

	nf = message.NewFieldInit(OBJECT_FIELD_NAME, message.Field_STRING, "")
	nf.AddValue(mssg.Object)
	pack.Message.AddField(nf)

	nf = message.NewFieldInit(LABELS_FIELD_NAME, message.Field_BYTES, "")
	lblBytes, _ := MarshalData(mssg.Labels)
	nf.AddValue(lblBytes)
	pack.Message.AddField(nf)

	nf = message.NewFieldInit(TYPE_FIELD_NAME, message.Field_STRING, "")
	nf.AddValue(mssg.Type)
	pack.Message.AddField(nf)

	nf = message.NewFieldInit(DATA_FIELD_NAME, message.Field_BYTES, "")
	dataBytes, _ := MarshalData(mssg.Data)
	nf.AddValue(dataBytes)
	pack.Message.AddField(nf)

	nf = message.NewFieldInit(TIMESTAMP_FIELD_NAME, message.Field_DOUBLE, "sec")
	nf.AddValue(mssg.Timestamp)
	pack.Message.AddField(nf)

	nf = message.NewFieldInit(TOKEN_FIELD_NAME, message.Field_BYTES, "")
	tokenBytes, _ := MarshalData(mssg.Token)
	nf.AddValue(tokenBytes)
	pack.Message.AddField(nf)

	return
}

// encodeSpecsMessage encodes a Heka message representing a SPECS internal message
// into a SPECS input message.
func encodeSpecsMessage(pack *pipeline.PipelinePack) (mssg *SpecsMonitoringMessage, ok bool) {
	mssg = new(SpecsMonitoringMessage)
	ok = true

	fieldValue, okp := pack.Message.GetFieldValue(COMPONENT_FIELD_NAME)
	if okp {
		mssg.Component = fieldValue.(string)
	}
	ok = ok || okp

	fieldValue, okp = pack.Message.GetFieldValue(OBJECT_FIELD_NAME)
	if okp {
		mssg.Object = fieldValue.(string)
	}
	ok = ok || okp

	fieldValue, okp = pack.Message.GetFieldValue(LABELS_FIELD_NAME)
	if okp {
		mssg.Labels, _ = UnmarshalJsonBytesToStringArray(fieldValue.([]byte))
	}
	ok = ok || okp

	fieldValue, okp = pack.Message.GetFieldValue(TYPE_FIELD_NAME)
	if okp {
		mssg.Type = fieldValue.(string)
	}
	ok = ok || okp

	fieldValue, okp = pack.Message.GetFieldValue(DATA_FIELD_NAME)
	if okp {
		mssg.Data, _ = UnmarshalJsonBytes(fieldValue.([]byte))
	}
	ok = ok || okp

	fieldValue, okp = pack.Message.GetFieldValue(TIMESTAMP_FIELD_NAME)
	if okp {
		mssg.Timestamp = fieldValue.(float64)
	}
	ok = ok || okp

	fieldValue, okp = pack.Message.GetFieldValue(TOKEN_FIELD_NAME)
	if okp {
		tokenPtr := new(MessageToken)
		err := UnmarshalJsonToStruct(fieldValue.([]byte), tokenPtr)
		if nil != err {
			okp = false
		} else {
			mssg.Token = *tokenPtr
		}
	}
	ok = ok || okp

	return
}

// encodeSpecsStremedEvent encodes a Heka message representing a SPECS internal message
// and its stream field into a SPECS streamed event.
func encodeSpecsStremedEvent(pack *pipeline.PipelinePack) (event *SpecsStreamedEvent, ok bool) {
	mssg := SpecsMonitoringMessage{}
	event = new(SpecsStreamedEvent)
	ok = true

	fieldValue, okp := pack.Message.GetFieldValue(COMPONENT_FIELD_NAME)
	if okp {
		mssg.Component = fieldValue.(string)
	}
	ok = ok || okp

	fieldValue, okp = pack.Message.GetFieldValue(OBJECT_FIELD_NAME)
	if okp {
		mssg.Object = fieldValue.(string)
	}
	ok = ok || okp

	fieldValue, okp = pack.Message.GetFieldValue(LABELS_FIELD_NAME)
	if okp {
		mssg.Labels, _ = UnmarshalJsonBytesToStringArray(fieldValue.([]byte))
	}
	ok = ok || okp

	fieldValue, okp = pack.Message.GetFieldValue(TYPE_FIELD_NAME)
	if okp {
		mssg.Type = fieldValue.(string)
	}
	ok = ok || okp

	fieldValue, okp = pack.Message.GetFieldValue(DATA_FIELD_NAME)
	if okp {
		mssg.Data, _ = UnmarshalJsonBytes(fieldValue.([]byte))
	}
	ok = ok || okp

	fieldValue, okp = pack.Message.GetFieldValue(TIMESTAMP_FIELD_NAME)
	if okp {
		mssg.Timestamp = fieldValue.(float64)
	}
	ok = ok || okp

	fieldValue, okp = pack.Message.GetFieldValue(TOKEN_FIELD_NAME)
	if okp {
		tokenPtr := new(MessageToken)
		err := UnmarshalJsonToStruct(fieldValue.([]byte), tokenPtr)
		if nil != err {
			okp = false
		} else {
			mssg.Token = *tokenPtr
		}
	}
	ok = ok || okp

	fieldValue, okp = pack.Message.GetFieldValue(STREAM_FIELD_NAME)
	if okp {
		event.Stream = fieldValue.(string)
	}
	event.MonitoringMessage = mssg

	return
}

func ClonePackMessage(src *message.Message) (mssg *message.Message) {
	mssg = message.CopyMessage(src)
	mssg.SetUuid(uuid.NewRandom())
	return
}

func (mt *MessageToken) Copy() (token *MessageToken) {
	token = new(MessageToken)
	token.UUID = mt.UUID
	token.SequenceNo = mt.SequenceNo
	return
}
