package core

import (
	"fmt"
	"reflect"
	"strconv"
	"strings"
)

// Sets the value of a field in a struct using the tag of the field for identifying the field.
func SetFieldByTag(elem interface{}, tagKey string, tagValue string, fieldValue string) (err error) {
	if !hasValidType(elem) {
		err = fmt.Errorf("Cannot use SetFieldByTag on a non-struct interface.")
		return
	}

	var (
		elemValue reflect.Value
		elemType  reflect.Type
	)

	if reflect.TypeOf(elem).Kind() == reflect.Ptr {
		elemValue = reflect.ValueOf(elem).Elem()
		elemType = reflect.TypeOf(elem).Elem()
	} else {
		elemValue = reflect.ValueOf(elem)
		elemType = reflect.TypeOf(elem)
	}

	for i := 0; i < elemType.NumField(); i++ {
		sft := elemType.Field(i)
		currentTagValue := sft.Tag.Get(tagKey)
		if strings.EqualFold(tagValue, currentTagValue) {
			fieldInstance := elemValue.Field(i)
			switch fieldInstance.Kind() {
			case reflect.Map, reflect.Ptr, reflect.Slice:
				data, err := UnmarshalJsonString(fieldValue)
				if nil == err {
					convertedVal := reflect.ValueOf(data).Convert(fieldInstance.Type())
					fieldInstance.Set(convertedVal)
				}
			case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
				len := reflect.TypeOf(fieldInstance.Kind()).Bits()
				data, err := strconv.ParseInt(fieldValue, 10, len)
				if nil == err {
					convertedVal := reflect.ValueOf(data).Convert(fieldInstance.Type())
					fieldInstance.Set(convertedVal)
				}
			default:
				convertedVal := reflect.ValueOf(fieldValue).Convert(fieldInstance.Type())
				fieldInstance.Set(convertedVal)
			}
		}
	}
	return
}

func hasValidType(obj interface{}) bool {
	types := []reflect.Kind{reflect.Struct, reflect.Ptr}
	for _, t := range types {
		if reflect.TypeOf(obj).Kind() == t {
			return true
		}
	}

	return false
}

func isStruct(obj interface{}) bool {
	return reflect.TypeOf(obj).Kind() == reflect.Struct
}

func isPointer(obj interface{}) bool {
	return reflect.TypeOf(obj).Kind() == reflect.Ptr
}

// Converts a slice of interface{} type into a slice of the given type.
func ConvertToTypeSlice(a interface{}, source interface{}, typ reflect.Type) (result interface{}) {
	aValue := reflect.ValueOf(a)

	sourceValue := reflect.ValueOf(reflect.Indirect(reflect.ValueOf(source)).Interface())

	elemTyp := typ.Elem()
	for i := 0; i < sourceValue.Len(); i++ {
		elem := sourceValue.Index(i).Elem().Convert(elemTyp)
		aValue = reflect.Append(aValue, elem)

	}

	aValuePtr := reflect.New(aValue.Type())
	aValuePtr.Elem().Set(aValue)
	result = reflect.Indirect(aValuePtr).Interface()
	return
}

// Converts recursively a map into a struct.
func ConvertMapToStruct(mapData map[string]interface{}, elem interface{}) (err error) {
	var (
		elemValue reflect.Value
		elemType  reflect.Type
	)

	if reflect.TypeOf(elem).Kind() == reflect.Ptr {
		elemValue = reflect.ValueOf(elem).Elem()
		elemType = reflect.TypeOf(elem).Elem()
	} else {
		elemValue = reflect.ValueOf(elem)
		elemType = reflect.TypeOf(elem)
	}

	for i := 0; i < elemType.NumField(); i++ {
		field := elemType.Field(i)
		fieldInstance := elemValue.Field(i)
		assignedVal := mapData[field.Name]
		if nil == assignedVal {
			assignedVal = mapData[field.Tag.Get("json")]
		}
		if nil == assignedVal {
			continue
		}
		if fieldInstance.Kind() == reflect.Struct {
			if reflect.ValueOf(assignedVal).Kind() == reflect.Map {
				pointer := reflect.New(field.Type).Interface()
				mapVal, ok := assignedVal.(map[string]interface{})
				if !ok {
					err = fmt.Errorf("[convertMap]: Value %s: %v is not of type map[string]interface{}.", field.Name, assignedVal)
					return
				}
				err = ConvertMapToStruct(mapVal, pointer)
				if nil != err {
					return
				}
				convertedVal := reflect.Indirect(reflect.ValueOf(pointer)).Convert(fieldInstance.Type())
				fieldInstance.Set(convertedVal)
			}
		} else if fieldInstance.Kind() == reflect.Slice || fieldInstance.Kind() == reflect.Array {
			iAssignedVal, ok := assignedVal.([]interface{})
			if !ok {
				err = fmt.Errorf("[convertMap] Value %s: %v is not of type []interface{}.", field.Name, assignedVal)
				return
			}

			cAssignedVal := reflect.MakeSlice(fieldInstance.Type(), 0, 0).Interface()
			result := ConvertToTypeSlice(cAssignedVal, &iAssignedVal, fieldInstance.Type())
			convertedVal := reflect.ValueOf(result).Convert(fieldInstance.Type())
			fieldInstance.Set(convertedVal)
		} else {
			convertedVal := reflect.ValueOf(assignedVal).Convert(fieldInstance.Type())
			fieldInstance.Set(convertedVal)
		}
	}

	return
}
