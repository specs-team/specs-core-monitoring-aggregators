package core

import (
	"encoding/base64"
	"fmt"
	"github.com/mozilla-services/heka/message"
	"github.com/mozilla-services/heka/pipeline"
	"net/http"
	"regexp"
	"strconv"
	"strings"
)

const (
	HANDLER_CONFIG_LINE_REGEXP = "handlers[.]([0-9]+)[.]([^=]*)=(.*)"
	MAX_HANDLERS               = 10
	DEFAULT_HTTP_METHOD        = "POST"

	ROUTER_INPUT_ACTION  = "input"
	ROUTER_OUTPUT_ACTION = "stream-output"

	ROUTER_REQUEST_PATH_FIELD_NAME         = "routerRequestPath"
	ROUTER_REQUEST_QUERY_FIELD_NAME        = "routerRequestQuery"
	ROUTER_HANDLER_PATH_PATTERN_FIELD_NAME = "routerHandlerPathPattern"
	ROUTER_HANDLER_ACTION_FIELD_NAME       = "routerHandlerAction"
	ROUTER_HANDLER_METHOD_FIELD_NAME       = "routerHandlerMethod"
	ROUTER_HANDLER_ENCODER_FIELD_NAME      = "routerHandlerEncoder"
	ROUTER_HANDLER_ANNOTATIONS_FIELD_NAME  = "routerHandlerAnnotations"
	ROUTER_REGISTER_FIELD_NAME             = "register_message"
	ROUTER_UNREGISTER_FIELD_NAME           = "unregister_message"
)

type RegisterMessage struct {
	Endpoint ClientEndpoint
	Method   string
	Streams  []string
	Encoder  string
}

type UnregisterMessage struct {
	Endpoint ClientEndpoint
	Streams  []string
}

type ClientEndpoint struct {
	Address string `json:"address"`
}

type RouterRequestHandlerConfig struct {
	PathPattern string                 `conf:"path_pattern"`
	Action      string                 `conf:"action"`
	StreamGroup int                    `conf:"path_pattern_stream_group"`
	Decoder     string                 `conf:"decoder"`
	Encoder     string                 `conf:"encoder"`
	Method      string                 `conf:"method"`
	Annotations map[string]interface{} `conf:"annotations"`
	pattern     *regexp.Regexp
	handler     http.HandlerFunc
}

var RouterBuffer *RouterStreamBuffers

func DecodeRouterHandlerConfiguration(rhConfigB64 string) (rHandlers []RouterRequestHandlerConfig, err error) {
	var (
		lines     []string
		multiline []string
		handlers  []*RouterRequestHandlerConfig
	)

	if "" == rhConfigB64 {
		err = fmt.Errorf("Empty router handlers configuration.")
		return
	}

	rhConfigBytes, err := base64.StdEncoding.DecodeString(rhConfigB64)
	if err != nil {
		err = fmt.Errorf("Cannot decode router handlers configuration: %s.", err)
		return
	}
	rhConfig := string(rhConfigBytes)

	handlers = make([]*RouterRequestHandlerConfig, MAX_HANDLERS)
	handlersCount := 0

	re := regexp.MustCompile(HANDLER_CONFIG_LINE_REGEXP)
	lines = strings.Split(rhConfig, "\n")

	for _, line := range lines {
		line = strings.TrimSpace(line)
		if nil == multiline {
			if strings.HasSuffix(line, "{") {
				multiline = make([]string, 32)
				multiline = append(multiline, line)
			}
		} else {
			multiline = append(multiline, line)
			if strings.HasSuffix(line, "}") {
				line = strings.Join(multiline, "")
				multiline = nil
			}
		}
		if nil != multiline {
			continue
		}
		matches := re.FindStringSubmatch(line)

		if 2 > len(matches) {
			continue
		}

		handlerNo, errconv := strconv.ParseInt(matches[1], 10, 16)
		if nil != errconv {
			err = fmt.Errorf("Handler configuration error: %s.", errconv)
			return
		}

		if nil == handlers[handlerNo] {
			handlers[handlerNo] = new(RouterRequestHandlerConfig)
			handlersCount++
		}
		handler := handlers[handlerNo]
		SetFieldByTag(handler, "conf", strings.TrimSpace(matches[2]), strings.TrimSpace(matches[3]))
	}

	rHandlers = make([]RouterRequestHandlerConfig, handlersCount)
	handlerNo := 0
	for _, handler := range handlers {
		if nil != handler {
			rHandlers[handlerNo] = *handler
			handlerNo++
		}
	}

	return
}

// AddFields adds custom fields to the Heka message, based on a SPECS register message.
// The resulted Heka message will be the SPECS register message.
func AddFieldsToRegisterMessage(pack *pipeline.PipelinePack, mssg *RegisterMessage) (err error) {
	nf := message.NewFieldInit(ROUTER_REGISTER_FIELD_NAME, message.Field_BYTES, "")
	dataBytes, _ := MarshalData(mssg)
	nf.AddValue(dataBytes)
	pack.Message.AddField(nf)

	return
}

// AddFields adds custom fields to the Heka message, based on a SPECS unregister message.
// The resulted Heka message will be the SPECS unregister message.
func AddFieldsToUnregisterMessage(pack *pipeline.PipelinePack, mssg *UnregisterMessage) (err error) {
	nf := message.NewFieldInit(ROUTER_UNREGISTER_FIELD_NAME, message.Field_BYTES, "")
	dataBytes, _ := MarshalData(mssg)
	nf.AddValue(dataBytes)
	pack.Message.AddField(nf)

	return
}
