package core

import (
	"code.google.com/p/go-uuid/uuid"
	"encoding/json"
	"fmt"
	"github.com/mozilla-services/heka/pipeline"
	"io"
	"reflect"
	"strings"
)

// SpecsDecoder decodes a SPECS input message into a SPECS internal message.
type SpecsDecoder struct {
	DecoderName string
	dRunner     pipeline.DecoderRunner
}

// Init tnitializes the decoder plugin. For more details,
// see https://godoc.org/github.com/mozilla-services/heka/pipeline#Plugin
func (sd *SpecsDecoder) Init(config interface{}) (err error) {
	return nil
}

// SetDecoderRunner provides the Decoder access to its DecoderRunner object
// when it is started. For more details, see https://godoc.org/github.com/mozilla-services/heka/pipeline#WantsDecoderRunner
func (sd *SpecsDecoder) SetDecoderRunner(dr pipeline.DecoderRunner) {
	sd.dRunner = dr
}

// SetName provides a plugin access to its configured name before it has started.
// For more details, see https://godoc.org/github.com/mozilla-services/heka/pipeline#WantsName
func (sd *SpecsDecoder) SetName(name string) {
	sd.DecoderName = name
}

// Decode extracts the raw message data from pack.MsgBytes and attempts
// to deserialize this and use the contained information to populate the
// Message struct pointed to by the pack.Message attribute.
// For more details, see https://godoc.org/github.com/mozilla-services/heka/pipeline#Decoder
func (sd *SpecsDecoder) Decode(pack *pipeline.PipelinePack) (packs []*pipeline.PipelinePack, err error) {
	var payload = pack.Message.GetPayload()
	sd.dRunner.LogMessage(fmt.Sprintf("Got message to decode %s\n", payload))

	packs = []*pipeline.PipelinePack{}
	dec := json.NewDecoder(strings.NewReader(payload))
	packCount := 0
	currentPack := pack
	for {
		mssg := new(SpecsMonitoringMessage)
		//errd := decodeMessage(dec, mssg)
		errd := DecoderUnmarshalJsonToStruct(dec, mssg)
		if errd == io.EOF {
			break
		} else if errd != nil {
			sd.dRunner.LogMessage(fmt.Sprintf("Invalid JSON message %s\n", errd))
			err = errd
			break
		}
		if packCount > 0 {
			currentPack = sd.dRunner.NewPack()
			currentPack.Message.SetUuid(uuid.NewRandom())
		}
		packs = append(packs, currentPack)

		mssgPayload, errd := buildMssgPayload(mssg)
		if errd != nil {
			sd.dRunner.LogMessage(fmt.Sprintf("%s\n", errd))
			err = errd
			mssgPayload = &payload
		}
		currentPack.Message.Payload = mssgPayload

		errd = AddFields(currentPack, mssg)
		if errd != nil {
			sd.dRunner.LogMessage(fmt.Sprintf("Could not add fields to message %s\n", errd))
			err = errd
		}

		specs := HEKA_EVENT_MESSAGE_TYPE
		currentPack.Message.Type = &specs
		packCount++
		//sd.dRunner.LogMessage(fmt.Sprintf("Decoded message[%s] with payload %s\n", uuid.UUID(currentPack.Message.Uuid).String(), currentPack.Message.GetPayload()))
	}
	//sd.dRunner.LogMessage(fmt.Sprintf("Decoded message %s. Created %d messages.\n", payload, packCount))
	return
}

func decodeMessage(dec *json.Decoder, mssg *SpecsMonitoringMessage) (err error) {
	err = dec.Decode(mssg)
	if err != nil {
		return
	}
	prototype := SpecsMonitoringMessage{}
	st := reflect.TypeOf(prototype)
	val := reflect.ValueOf(mssg).Elem()
	for i := 0; i < st.NumField(); i++ {
		field := st.Field(i)
		valField := val.FieldByName(field.Name)
		switch valField.Kind() {
		case reflect.Chan, reflect.Func, reflect.Map, reflect.Ptr, reflect.Slice, reflect.UnsafePointer:
			if 0 == valField.Pointer() {
				err = fmt.Errorf("Missing field '%s'.", field.Name)
			}
		default:
			if valField.Interface() == reflect.Zero(valField.Type()).Interface() {
				err = fmt.Errorf("Missing field '%s'.", field.Name)
			}
		}
		if err != nil {
			break
		}
	}

	return
}

func buildMssgPayload(mssg *SpecsMonitoringMessage) (payload *string, err error) {
	bytes, e := json.Marshal(mssg)
	if e != nil {
		err = fmt.Errorf("Could not deserialiaze JSON %+v. Error: %s", mssg, e)
		return
	}
	str := fmt.Sprintf("%s", bytes)
	payload = &str
	return
}

func init() {
	pipeline.RegisterPlugin("SpecsDecoder", func() interface{} {
		return new(SpecsDecoder)
	})
}
