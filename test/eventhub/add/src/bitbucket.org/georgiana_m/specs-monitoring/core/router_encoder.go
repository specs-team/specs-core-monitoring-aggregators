package core

import (
	"encoding/json"
	"fmt"
	"github.com/mozilla-services/heka/pipeline"
)

// RouterEncoder encodes a Heka message representing a SPECS internal message
// and its stream into a SPECS streamed event.
type RouterEncoder struct {
}

// Init initializes the encoder plugin. For more details,
// see https://godoc.org/github.com/mozilla-services/heka/pipeline#Plugin
func (se *RouterEncoder) Init(config interface{}) (err error) {
	return
}

// Encode encodes the Heka message representing a SPECS internal message
// into a SPECS streamed event. For more details,
// see https://godoc.org/github.com/mozilla-services/heka/pipeline#Encoder
func (se *RouterEncoder) Encode(pack *pipeline.PipelinePack) (output []byte, err error) {
	mssg, ok := encodeSpecsStremedEvent(pack)
	if !ok {
		err = fmt.Errorf("Some fields of the output Specs JSON may be missing.")
	}

	payload, merr := json.Marshal(mssg)
	if merr != nil {
		err = fmt.Errorf("Could not serialiaze JSON %+v. Error: %s", mssg, merr)
		return
	}
	// Just payload
	output = []byte(payload)

	return
}

func init() {
	pipeline.RegisterPlugin("RouterEncoder", func() interface{} {
		return new(RouterEncoder)
	})
}
