#SPECS monitoring: The Event Hub

This repository contains the [SPECS](http://www.specs-project.eu/) Event Hub monitoring module. The general architecture of the Event Hub can be found [here](https://drive.google.com/file/d/0B1MQFBCcH4LseXBJY2p6UUp4eGM/view?usp=sharing). This is a work in progress and as such the current version does not contain all components in the link. The currently implemented components are described below. 

##Table Of Contents
1. [Installing the Event Hub](#markdown-header-installing-the-event-hub)
	
1. [Configuring the Event Hub](#markdown-header-configuring-the-event-hub)
	2. [Core configuration](#markdown-header-core-configuration)

1. [Using the Event Hub](#markdown-header-using-the-event-hub)
	2. [Starting the Hub](#markdown-header-starting-the-hub)
	2. [Sending messages](#markdown-header-sending-messages)
	2. [Receiving events](#markdown-header-receiving-events) 

1. [SPECS messages](#markdown-header-specs-messages)
	2. [Input and Event message](#markdown-header-input-and-event-message)
	2. [Event Hub's internal message](#markdown-header-event-hubs-internal-message)
  
1. [SPECS monitoring plugins](#markdown-header-specs-monitoring-plugins)
	2. [Decoders](#markdown-header-decoders)
		- [SpecsDecoder](#markdown-header-specsdecoder)
	2. [Filters](#markdown-header-filters)
		- [SieveFilter](#markdown-header-sievefilter)
		- [SpecsCounterFilter](#markdown-header-specscounterfilter)

    
#Installing the Event Hub
The Event Hub requires a Go work environment to be setup for the binary to be built. Prior to installing the Hub you must install and configure Go (the GOPATH and GOROOT environment variables must also be set).

The Hub is implemented using [Mozilla Heka](https://hekad.readthedocs.org/en/latest/index.html) version 0.10.0 and therefore requires Heka to be setup for building the binary. However, you will not need to install Heka as this task is automated for you by the Hub's build process. Because the Hub is based on Heka, all Heka prerequisites must be available. Please check [Heka's installation instructions](https://hekad.readthedocs.org/en/latest/installing.html) for its list of prerequisites.

Once you have all prerequisites, you can follow with the Event Hub's installation. For this all you need is the _install.sh_ script in this repository. You do not need to download anything else. Copy the _install.sh_ somewhere on your computer and run it.
Upon its successful execution, in the same folder where you executed the script you should find an executable file named _event-hub_. 

#Configuring the Event Hub
The Event Hub configuration file specifies how to handle received messages and requests for event notifications, and and also what Sieve Filters to load (check the [Sieve Filter section](#markdown-header-sievefilter) for more details on it). The configuration file is in [TOML](https://github.com/toml-lang/toml) format. 

The configuration file is broken into sections. There is a section named _hekad_ which contains global configuration options described [here](https://hekad.readthedocs.org/en/latest/config). To the configurations options described there, the Event Hub adds two other described in the [next section](#markdown-header-core-configuration). 

Besides the global configuration section, the configuration file will contain a section for each instance of a plugin which must be loaded by the Event Hub. The section name specifies the name of the plugin, and the “type” parameter specifies the plugin type. For the Event Hub the only accepted plugin types are [_SieveFilter_](#markdown-header-sievefilter), [_SpecsCounterFilter_](#markdown-header-specscounterfilter) and [_SpecsDecoder_](#markdown-header-specsdecoder). The configuration options for each of these plugins are presented in their respective sections.

##Core configuration
The Event Hub's core configuration extends the standard Heka's global configuration.
The Hub has two related configuration options:

- *buffer\_size* (int, optional): The Event Hub temporarily stores events in buffers before sending them to the interested clients. This option specifies the size of these buffers. Defaults to 1024.
- *handlers\_configuration* (string, required): This is a base64 encoded string which specifies how input messages and outputs should be handled. 
- *port* (int, optional): The port on which the Event Hub will receive HTTP requests. Defaults to 8080.

Clients can send input messages to the Event Hub using HTTP. The output events routed through the Hub can be received by interested clients also using HTTP. The Hub uses this _handler\_configuration_ in order to determine which HTTP requests are sending input messages to the Hub and which are actually requesting events from the Hub. As such, a handler is composed of a set of parameters as follows:

- `path_pattern`: a regular expression for the HTTP request URI path. This path will be checked against the received request's path and, in case of a match, the rest of the handler's parameters will specify how to handle the request. This is a mandatory parameter.
- `action`: this will indicate if this is an input message or a request for receiving events from the Hub. This is a mandatory parameter. The only possible values are _input_ and _stream\_output_.
- `method`: an HTTP method. For now, this should be POST when sending input messages and GET when requesting messages from the Hub. This is a mandatory parameter.
- `path_pattern_stream_group`: an integer value indicating which group from the regular expression in the first parameters contains a stream name. For more details on this parameter see section [Receiving events](#markdown-header-receiving-events). This is mandatory when the `action` parameter is _stream\_output_.
- `decoder`: the decoder used for decoding input messages. This should correspond to the name of a Heka decoder plugin. For now, the only plugin which can be used is the _SpecsDecoder_. This option is mandatory when the `action` parameter is _input_ and is mutually exclusive with the `encoder` parameter.
- `encoder`: the encoder used for encoding events. For now, the only encoder to be used is _SpecsEventEncoder_. This option is mandatory when the `action` parameter is _stream\_output_ and is mutually exclusive with the `decoder` parameter.
- `annotations`: a key-value mapping. This is not currently used, but was defined because it will be used by the _SpecsRestDecoder_ and _SpecsRestEncoder_, depicted in the linked component diagram and which will be implemented in the next version of the Hub. 
   
The base64 decoded handlers must comply to the following syntax:

	handlers.<index>.path_pattern = <regular expression describing a HTTP request URI path>
	handlers.<index>.action = input|stream\_output
	handlers.<index>.method = POST|GET
	handlers.<index>.path_pattern_stream_group = <integer value>
	handlers.<index>.decoder = SpecsDecoder
	handlers.<index>.encoder = SpecsEventEncoder

The _<index\>_ placeholder is an integer value used for identifying the parameters of a single handler. As mentioned above, a handler will not have all the parameters in the syntax description, only those related to its associated action.

The example below defines two handlers, one for input and one for output streaming. The handler with index 0 is an input handler with an associated request URI path starting with _/events/_. The handler with index 1 is an output handler with an associated request URI path starting with _/streams/_. The handler gives 1 as the _path\_pattern\_stream\_group_ which means that whatever the client will put in the request URI path between _/stream/_ and _/consume_ will be interpreted as a stream name and only events marked with that stream will be sent to the client.  

_Example handlers configuration:_

	handlers.0.path_pattern = ^/events/([^/]+)$
	handlers.0.action = input
	handlers.0.method = POST
	handlers.0.decoder = SpecsDecoder

	handlers.1.path_pattern = ^/streams/([^/]+)/consume$
	handlers.1.action = stream-output
	handlers.1.path_pattern_stream_group = 1
	handlers.1.encoder = SpecsEventEncoder

#Using the Event Hub
##Starting the Hub
You can start an Event Hub process based on a certain configuration by using the command:

	event-hub -config=<toml file>

##Sending messages
In order to send a message to the Event Hub, a client should send a HTTP POST request with a path matching one of the Hub's input handlers path pattern. The request should contain a SPECS input message conforming to the format described in section [Input message](#markdown-header-input-message).

For example, if the Hub is configured with the handlers in the example in section [Core configuration](#markdown-header-core-configuration) and listens on the endpoint localhost:8080, a valid request URI would be _http://localhost:8080/events/whatever_.  

##Receiving events
A client that wants to be receive events from the Event Hub, must send a HTTP GET request with a path matching one of the Hub's output handlers path pattern. 

For example, if the Hub is configured with the handlers in the example in section [Core configuration](#markdown-header-core-configuration) and listens on the endpoint localhost:8080, a valid request URI would be _http://localhost:8080/streams/specs/consume_. Once the connection is established the Event Hub will start streaming events to the client, where all events are marked with the stream _specs_. The format of the output event is the same as the input message. 

Each event has a _token_ attribute which uniquely identifies the message. This token can be used by the client when requesting events. To understand better how it can be used, lets take the following scenario. The client connects to the Hub using the above URI and starts receiving events from the Hub. During the streaming, something happens and the connection is dropped. When the client reconnects, it will want to receive only the events generated after the connection was dropped. To be able to handle this scenario, the Hub keeps a buffer of the latest messages with each stream. Each buffer has an associated UUID. Each event in the buffer has a token composed from this UUID and a sequence number. This token is sent by the Hub to the client, in the event. The events are sent ordered by sequence number. When connection between client and Hub drops, the client knows which was the last event it received and all it has to do is to send the token in this event to the Hub when it reconnects. The serialized token can be send in the _SPECS-Heka-HTTP-Gateway-Stream-Sequence_ request header. By serialized token we understand here the token in the form UUID:sequence\_number. If the Hub receives this header and it was not restarted in the meantime, it will send only events with sequence number greater than the one in the token. If it was restarted, it will send all events in the respective stream buffer. 

#SPECS messages
##Input and Event message
The monitoring events are received by the Event Hub as JSON messages. An input message received by the Hub may contain several such JSON messages. The events generated as output by the Hub will follow the same JSON format.

The JSON message contains the following attributes:

- `component` (string): the unique identifier (usually a UUID) of the component instance which generated the event, i.e. RabbitMQ instance or a web-server instance
- `object` (string): an hierarchical string indicating the observed object, i.e. queue, exchange, etc.
- `labels` (array of strings): a list of hierarchical strings used for categorizing components and objects, like "app-1.frontend-service" or "app-1.backend-service"
- `type` (string): an hierarchical string indicating what type of event is this, like "syslog", "structured-log", "cloudwatch.metrics"
- `data` (JSON object): depends on the type of event
- `timestamp` (number): time of the event, in seconds
- `token` (JSON object): the token identifying an event generated by the Event Hub. The object has two attributes `uuid`, containing the UUID in the token, and `seq`, containing the sequence number in the token.

##Event Hub's internal message
The input message is decoded by the decoder associated with the Hub's selected handler into a custom [Heka message](https://hekad.readthedocs.org/en/latest/message/index.html) which will be called from now on internal message. Each JSON message in the input message will be decoded into an internal message. 
For the internal messages the `type` message variable of the Heka messages will be set to __specs.monitoring.event__ and the `payload` message variable will contain the input JSON message.
For each attribute in the input JSON message, the internal message contains a field variable. Thus, the message contains the following fields (in brackets is the Go type of the field:

- `component` (string)
- `object` (string)
- `labels` ([]string)
- `type` (string)
- `data` (interface{})
- `timestamp` (float64)
- `token` (a custom struct type)

#SPECS monitoring plugins
This section describes the Heka plugins developed for the Event Hub. For more details on each kind of Heka plugin, please check the [official Heka documentation](http://hekad.readthedocs.org/en/latest/).

##Decoders
###SpecsDecoder
Parses a payload containing one or more JSON input messages and builds an Event Hub's internal message for each JSON message.

If your Event Hub _handler\_configuration_ contains a reference to this decoder, the Hub will automatically configure and start an instance of the decoder for you.

But you may start additional decoders using your configuration file.

_Example Heka configuration:_
   
	[TheSpecsDecoder]
	type = "SpecsDecoder"

##Filters
###SieveFilter
This filter adds a field called `stream` to all Heka messages it receives. This field is used in the Event Hub for further routing of messages to specific outputs. If the configuration used for starting Heka contains more than one _SieveFilter_, each filter will create a copy of the received message in which will add the _stream_ field giving it the value of the filter's stream value. If the received message already has the field _stream_, then it will replace its value. The copied message will be injected back in the Hub.
  
During configuration of the filter, one should configure the *message\_matcher* option such that the filter will not receive its own messages. Specifically, if the configured _stream_ for a filter is "abc", then its *message\_matcher* must contain *Fields[stream] != 'abc' || Fields[stream] == NIL* 

_Config:_ 

Besides the configuration options that are universally available to all Heka filter plugins, this plugin also supports the following configuration options:

- *message\_matcher* (string, optional): This is a standard option, but we added it in this description in order to specify its default value: `Fields[stream] != NIL`.
- *stream* (string, required): Specifies the value of the `stream` field to be appended to the messages.

_Example Heka configuration:_

    [TheSieveFilter]
    type = "SpecsSieveFilter"
    message_matcher = "Fields[component] == 'component1' && (Fields[stream] != 'demo' || Fields[stream] == NIL)"
    stream_id = "demo"

###SpecsCounterFilter
Once per ticker interval a SpecsCounterFilter will generate one or more Event Hub's internal messages. If the *group\_messages\_by* configuration option was left empty, the filter will generate a single message indicating the number of messages that matched the filter’s *message\_matcher* value during that interval (i.e. it counts the messages the plugin received). If the *group\_messages\_by* configuration option was set, then the filter will group the messages received by the plugin based on the value of the field in the *group\_messages\_by* option, and once per ticker interval will generate one message for each such value, counting how many messages were received for each value of the grouping field encountered.

The generated internal messages will have the Heka type *specs.counter.v1*. 

The `label` field of the message will contain a single string with the value of the field used for grouping messages, i.e. component names, object names, event types.

The `data` field will have the structure:

	Operator string
	Result   string

_Config:_

Besides the configuration options that are universally available to all Heka filter plugins, this plugin also supports the following configuration options:

- *group\_messages\_by* (string, optional): Specifies based on which input message field to group messages for counting. Valid values are: _component_, _object_ and _type_. If the option is missing or is the empty string all SPECS messages in a ticker interval will be counted.
- *message\_matcher* (string, optional): This is a standard option, but we added it in this description in order to specify its default value: `Type != 'specs.counter.v1'`.
- *ticker\_interval* (int, optional): Interval between generated counter messages, in seconds. Defaults to 300. (This is a standard option, but we added it in this description in order to specify its default value). 

_Example Heka configuration:_

	[TheSpecsCounterFilter]
	type = "SpecsCounterFilter"
	message_matcher = "Fields[component] != NIL && Type != 'specs.counter.v1'"
	ticker_interval = 10
	group_messages_by="component"

_Example Event Hub's internal output message payloads:_


```
#!json
{
	"component":"specs-counter-filter",
	"object":"",
	"labels":["component1"],
	"type":"specs.aggregation",
	"data":{
		"operator":"count",
		"result":"2"
	},
	"timestamp":1414701485
}

{
	"component":"specs-counter-filter",
	"object":"",
	"labels":["component2"],
	"type":"specs.aggregation",
	"data":{
		"rperator":"count",
		"result":"2"
	},
	"timestamp":1414701485
}
```



