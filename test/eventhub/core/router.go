package core

import (
	"fmt"
	"github.com/mozilla-services/heka/message"
	"github.com/mozilla-services/heka/pipeline"
	"io"
	"strings"
)

const (
	ROUTER_INPUT_NAME   = "SpecsEventRouter"
	ROUTER_DECODER_NAME = "SpecsRouterMultiDecoder"
)

// This EventRouter is an Input. It opens a channel and waits for messages. Each message is actually a http.Request.
// Each request will be handled, based on the path of the request.
type EventRouter struct {
	config          *EventRouterConfig
	pipelineConfig  *pipeline.PipelineConfig
	inChan          chan *HttpRequest
	inputRunner     pipeline.InputRunner
	requestHandlers []RouterRequestHandlerConfig
}

// EventRouterConfig config struct is used for defining and specifying default
// values for input's custom parameters.
type EventRouterConfig struct {
	// Specifies the configuration of the RouterRequestHandler(s) used by the router.
	Configuration string `toml:"configuration"`
	// Decoder to be used by the input. So we can default to using RouterMultiDecoder.
	Decoder string `toml:"decoder"`
	// Splitter to be used by the input. So we can default to using NullSplitter.
	Splitter string `toml:"splitter"`
}

// SetPipelineConfig gives access to the active PipelineConfig or
// GlobalConfigStruct values in the ConfigStruct or Init methods. For more details,
// see https://godoc.org/github.com/mozilla-services/heka/pipeline#WantsPipelineConfig
func (rr *EventRouter) SetPipelineConfig(pConfig *pipeline.PipelineConfig) {
	rr.pipelineConfig = pConfig
}

// ConfigStruct returns a default-value-populated configuration structure into which
// the plugin's TOML configuration will be deserialized. For more details,
// see https://godoc.org/github.com/mozilla-services/heka/pipeline#HasConfigStruct
func (rr *EventRouter) ConfigStruct() interface{} {
	return &EventRouterConfig{
		Decoder:  ROUTER_DECODER_NAME,
		Splitter: "NullSplitter",
	}
}

// Init initializes the plugin. For more details,
// see https://godoc.org/github.com/mozilla-services/heka/pipeline#Plugin
func (rr *EventRouter) Init(config interface{}) (err error) {
	rr.config = config.(*EventRouterConfig)
	rr.inChan = make(chan *HttpRequest, 32)

	rr.requestHandlers, err = DecodeRouterHandlerConfiguration(rr.config.Configuration)
	if nil != err {
		return
	}

	return nil
}

func (rr *EventRouter) Run(ir pipeline.InputRunner, h pipeline.PluginHelper) (err error) {
	rr.inputRunner = ir

	for pack := range rr.inChan {
		//ir.LogMessage(fmt.Sprintf("Received message %+v: ", pack))
		rr.handleRequest(pack)
	}

	return nil
}

func (rr *EventRouter) Stop() {
	close(rr.inChan)
}

func (rr *EventRouter) InChan() chan *HttpRequest {
	return rr.inChan
}

func (rr *EventRouter) handleRequest(req *HttpRequest) {
	sRunner := rr.inputRunner.NewSplitterRunner(req.RemoteAddr)
	if !sRunner.UseMsgBytes() {
		sRunner.SetPackDecorator(rr.makePackDecorator(*req))
	}
	var (
		record  []byte
		err     error
		deliver bool
	)
	//for err == nil {
	// TODO find out why we don't get EOF
	deliver = true
	bodyReader := strings.NewReader(string(req.Body))
	_, record, err = sRunner.GetRecordFromStream(bodyReader)
	if err == io.ErrShortBuffer {
		if sRunner.KeepTruncated() {
			err = fmt.Errorf("record exceeded MAX_RECORD_SIZE %d and was truncated",
				message.MAX_RECORD_SIZE)
		} else {
			deliver = false
			err = fmt.Errorf("record exceeded MAX_RECORD_SIZE %d and was dropped",
				message.MAX_RECORD_SIZE)
		}
		rr.inputRunner.LogError(err)
		err = nil // non-fatal, keep going
	}
	if len(record) > 0 && deliver {
		sRunner.DeliverRecord(record, nil)
	}
	//}
	if nil != err && err != io.EOF {
		rr.inputRunner.LogError(fmt.Errorf("[Router] receiving request body: %s", err.Error()))
	}
}

func (rr *EventRouter) makePackDecorator(req HttpRequest) func(*pipeline.PipelinePack) {
	packDecorator := func(pack *pipeline.PipelinePack) {
		pack.Message.SetType(HEKA_EVENT_MESSAGE_TYPE)
		if field, err := message.NewField("Protocol", req.Proto, ""); err == nil {
			pack.Message.AddField(field)
		} else {
			rr.inputRunner.LogError(fmt.Errorf("[Router] can't add field: %s", err))
		}
		if field, err := message.NewField("ContentType", req.Header.Get("Content-Type"), ""); err == nil {
			pack.Message.AddField(field)
		} else {
			rr.inputRunner.LogError(fmt.Errorf("[Router] can't add field: %s", err))
		}
		if field, err := message.NewField("Method", req.Method, ""); err == nil {
			pack.Message.AddField(field)
		} else {
			rr.inputRunner.LogError(fmt.Errorf("[Router] can't add field: %s", err))
		}
		if field, err := message.NewField(ROUTER_REQUEST_PATH_FIELD_NAME, req.URL.Path, ""); err == nil {
			pack.Message.AddField(field)
		} else {
			rr.inputRunner.LogError(fmt.Errorf("[Router] can't add field: %s", err))
		}
		if field, err := message.NewField(ROUTER_REQUEST_QUERY_FIELD_NAME, req.URL.RawQuery, ""); err == nil {
			pack.Message.AddField(field)
		} else {
			rr.inputRunner.LogError(fmt.Errorf("[Router] can't add field: %s", err))
		}
		if field := message.NewFieldInit(ANNOTATIONS_FIELD_NAME, message.Field_BYTES, ""); field != nil {
			field.AddValue(req.Header.Get(ANNOTATIONS_HEADER_NAME))
			pack.Message.AddField(field)
		} else {
			rr.inputRunner.LogError(fmt.Errorf("[Router] can't add field: %s", ANNOTATIONS_FIELD_NAME))
		}
	}
	return packDecorator
}

func init() {
	pipeline.RegisterPlugin("EventRouterInput", func() interface{} {
		return new(EventRouter)
	})
}
