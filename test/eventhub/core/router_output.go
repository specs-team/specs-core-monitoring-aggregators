package core

import (
	"fmt"
	"github.com/mozilla-services/heka/pipeline"
)

const (
	ROUTER_OUTPUT_NAME  = "SpecsRouterOutput"
	ROUTER_ENCODER_NAME = "SpecsRouterEncoder"
)

type RouterOutput struct {
	OutputName     string
	config         *RouterOutputConfig
	pipelineConfig *pipeline.PipelineConfig
	oRunner        pipeline.OutputRunner
}

type RouterOutputConfig struct {
	// Encoder to be used by the output. Default is SpecsRouterEncoder.
	Encoder string `toml:"encoder"`
	// Boolean expression, when evaluated to true passes the message to the
	// filter for processing. Defaults to messages of types 'specs.monitoring.event',
	// 'specs.monitoring.register.client' and 'specs.monitoring.unregister.client'.
	MessageMatcher string `toml:"message_matcher"`
}

func (ro *RouterOutput) ConfigStruct() interface{} {
	return &RouterOutputConfig{
		Encoder:        ROUTER_ENCODER_NAME,
		MessageMatcher: "Type == 'specs.monitoring.event' && Fields[stream] != NIL",
	}
}

// SetPipelineConfig gives access to the active PipelineConfig or
// GlobalConfigStruct values in the ConfigStruct or Init methods. For more details,
// see https://godoc.org/github.com/mozilla-services/heka/pipeline#WantsPipelineConfig
func (ro *RouterOutput) SetPipelineConfig(pConfig *pipeline.PipelineConfig) {
	ro.pipelineConfig = pConfig
}

// SetName provides a plugin access to its configured name before it has started.
// For more details, see https://godoc.org/github.com/mozilla-services/heka/pipeline#WantsName
func (ro *RouterOutput) SetName(name string) {
	ro.OutputName = name
}

func (ro *RouterOutput) Init(config interface{}) (err error) {
	ro.config = config.(*RouterOutputConfig)
	return
}

func (ro *RouterOutput) Run(oRunner pipeline.OutputRunner, h pipeline.PluginHelper) (err error) {
	ro.oRunner = oRunner
	inChan := oRunner.InChan()

	for pack := range inChan {
		mssg := pack.Message
		if *mssg.Type == HEKA_EVENT_MESSAGE_TYPE {
			//oRunner.LogMessage(fmt.Sprintf("[%s] received message %+v", ro.OutputName, pack.Message))
			//if outBytes, e = oRunner.Encode(pack); e != nil {
			//	oRunner.LogError(e)
			//} else if outBytes != nil {

			fieldValue, ok := pack.Message.GetFieldValue(STREAM_FIELD_NAME)
			if ok {
				event, ok := encodeSpecsStremedEvent(pack)
				if !ok {
					err = fmt.Errorf("Some fields of the output Specs JSON may be missing.")
				}

				stream := fieldValue.(string)
				// put bytes in buffer
				RouterBuffer.Enqueue(stream, event)
			} else {
				oRunner.LogError(fmt.Errorf("[%s] received message without STREAM field.", ro.OutputName))
			}
			//}

			pack.Recycle()
		} else {
			pack.Recycle()
		}
	}

	ro.Stop()

	return
}

func (ro *RouterOutput) Stop() {
	if nil != ro.oRunner {
		ro.oRunner.LogMessage(fmt.Sprintf("[%s] stopping", ro.OutputName))
	}

	return
}

func init() {
	pipeline.RegisterPlugin("RouterOutput", func() interface{} {
		return new(RouterOutput)
	})
}
