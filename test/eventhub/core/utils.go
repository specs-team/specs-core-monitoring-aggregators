package core

import (
	"github.com/georgiana-m/bt_toml"
	"os"
	"sort"
)

func Compare(s1 []string, s2 []string) bool {
	if (nil == s1 && nil != s2) || (nil != s1 && nil == s2) {
		return false
	}
	if len(s1) != len(s2) {
		return false
	}
	sort.Strings(s1)
	sort.Strings(s2)
	for index, elem := range s1 {
		if elem != s2[index] {
			return false
		}
	}
	return true
}

func Diff(s1 []string, s2 []string) []string {
	var result []string
	if nil == s1 {
		return result
	}
	if nil == s2 || len(s2) == 0 {
		for _, elem := range s1 {
			result = append(result, elem)
		}
		return result
	}

	for _, elem := range s1 {
		if !Contains(s2, elem) {
			result = append(result, elem)
		}
	}
	return result
}

func Contains(s []string, elem string) bool {
	for _, a := range s {
		if a == elem {
			return true
		}
	}
	return false
}

// WriteTomlFile creates a TOML file for the configuration specified in the map parameter.
func WriteTomlFile(filePath string, config map[string]interface{}) (err error) {
	var (
		f   *os.File
		enc *toml.Encoder
	)
	f, err = os.Create(filePath)
	defer f.Close()

	if err != nil {
		return
	}

	enc = toml.NewEncoder(f)
	err = enc.Encode(config)
	if err != nil {
		return
	}

	err = f.Sync()
	if err != nil {
		return
	}
	return
}
