package core

import (
	"code.google.com/p/go-uuid/uuid"
	"fmt"
	"github.com/mozilla-services/heka/pipeline"
	"github.com/zfjagann/golang-ring"
	"sort"
	"sync"
)

type RouterStreamBuffers struct {
	buffers map[string]*ring.Ring
	tokens  map[string]MessageToken
	lock    sync.Mutex
}

func NewRouterStreamBuffers(size int) (rb *RouterStreamBuffers) {
	ring.DefaultCapacity = size
	buffers := make(map[string]*ring.Ring)
	tokens := make(map[string]MessageToken)
	rb = &RouterStreamBuffers{
		buffers: buffers,
		tokens:  tokens,
	}
	return
}

func (rb *RouterStreamBuffers) Enqueue(stream string, elem *SpecsStreamedEvent) {
	rb.lock.Lock()
	ring, ok := rb.buffers[stream]
	if !ok {
		rb.createStreamBuffer(stream)
		ring = rb.buffers[stream]
	}
	token := rb.tokens[stream]
	token.SequenceNo = token.SequenceNo + 1
	rb.tokens[stream] = token
	elem.MonitoringMessage.Token = *(token.Copy())
	ring.Enqueue(elem)
	rb.lock.Unlock()
}

func (rb *RouterStreamBuffers) GetToken(stream string) (token MessageToken) {
	rb.lock.Lock()
	token, _ = rb.tokens[stream]
	rb.lock.Unlock()
	return
}

func (rb *RouterStreamBuffers) Values(stream string, lastToken *MessageToken) []*SpecsStreamedEvent {
	var values []*SpecsStreamedEvent
	rb.lock.Lock()
	ring, ok := rb.buffers[stream]
	if !ok {
		values = []*SpecsStreamedEvent{}
	} else {
		vals := ring.Values()
		for index, _ := range vals {
			if event, ok := vals[index].(*SpecsStreamedEvent); ok {
				if nil == lastToken {
					values = append(values, event)
				} else if lastToken.SequenceNo < event.MonitoringMessage.Token.SequenceNo {
					values = append(values, event)
				}
			} else {
				pipeline.LogError.Println(fmt.Errorf("Cannot convert to SpecsStreamedEvent."))
			}
		}
		sort.Sort(BySequenceNo(values))
	}
	rb.lock.Unlock()
	return values
}

func (rb *RouterStreamBuffers) createStreamBuffer(stream string) {
	ring := new(ring.Ring)
	rb.buffers[stream] = ring
	tokenUUID := uuid.NewRandom()
	token := MessageToken{UUID: tokenUUID, SequenceNo: 0}
	rb.tokens[stream] = token
}

// BySequenceNo implements sort.Interface for []*SpecsStreamedEvent based on
// the MonitoringMessage.Token.SequenceNo field.
type BySequenceNo []*SpecsStreamedEvent

func (a BySequenceNo) Len() int {
	return len(a)
}

func (a BySequenceNo) Swap(i, j int) {
	a[i], a[j] = a[j], a[i]
}

func (a BySequenceNo) Less(i, j int) bool {
	return a[i].MonitoringMessage.Token.SequenceNo < a[j].MonitoringMessage.Token.SequenceNo
}
