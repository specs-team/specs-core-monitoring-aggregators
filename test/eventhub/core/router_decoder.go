package core

import (
	"fmt"
	"github.com/mozilla-services/heka/message"
	"github.com/mozilla-services/heka/pipeline"
	"regexp"
)

// RouterMultiDecoder is not a general purpose decoder. It is meant to be used
// with an EventRouter input type. It decodes a request received by the EventRouter and
// sends it for the actual decoding to the decoder specified in the handler
// corresponding to the request path.
// The correct decoder is determined based on the path in the received HTTP request.
type RouterMultiDecoder struct {
	DecoderName     string
	Decoders        map[string]pipeline.Decoder
	pConfig         *pipeline.PipelineConfig
	config          *RouterMultiDecoderConfig
	dRunner         pipeline.DecoderRunner
	requestHandlers []RouterRequestHandlerConfig
}

type RouterMultiDecoderConfig struct {
	// Specifies the configuration of the RouterRequestHandler(s) used by the router.
	Configuration string `toml:"configuration"`
}

// ConfigStruct returns a default-value-populated configuration structure into which
// the plugin's TOML configuration will be deserialized. For more details,
// see https://godoc.org/github.com/mozilla-services/heka/pipeline#HasConfigStruct
func (rmd *RouterMultiDecoder) ConfigStruct() interface{} {
	return &RouterMultiDecoderConfig{}
}

// Init tnitializes the decoder plugin. For more details,
// see https://godoc.org/github.com/mozilla-services/heka/pipeline#Plugin
func (rmd *RouterMultiDecoder) Init(config interface{}) (err error) {
	rmd.config = config.(*RouterMultiDecoderConfig)
	rmd.requestHandlers, err = DecodeRouterHandlerConfiguration(rmd.config.Configuration)
	if nil != err {
		return
	}

	rmd.Decoders = make(map[string]pipeline.Decoder)

	return nil
}

// Heka will call this before calling any other methods to give us access to
// the pipeline configuration.
func (rmd *RouterMultiDecoder) SetPipelineConfig(pConfig *pipeline.PipelineConfig) {
	rmd.pConfig = pConfig
}

// SetDecoderRunner provides the Decoder access to its DecoderRunner object
// when it is started. For more details, see https://godoc.org/github.com/mozilla-services/heka/pipeline#WantsDecoderRunner
func (rmd *RouterMultiDecoder) SetDecoderRunner(dr pipeline.DecoderRunner) {
	rmd.dRunner = dr
}

// SetName provides a plugin access to its configured name before it has started.
// For more details, see https://godoc.org/github.com/mozilla-services/heka/pipeline#WantsName
func (rmd *RouterMultiDecoder) SetName(name string) {
	rmd.DecoderName = name
}

// Decode extracts the raw message data from pack.MsgBytes and attempts
// to deserialize this and use the contained information to populate the
// Message struct pointed to by the pack.Message attribute.
// For more details, see https://godoc.org/github.com/mozilla-services/heka/pipeline#Decoder
func (rmd *RouterMultiDecoder) Decode(pack *pipeline.PipelinePack) (packs []*pipeline.PipelinePack, err error) {
	var (
		decoder  pipeline.Decoder
		rhConfig RouterRequestHandlerConfig
	)

	path := pack.Message.FindFirstField(ROUTER_REQUEST_PATH_FIELD_NAME)
	if nil == path {
		err = fmt.Errorf("[%s] The package does not contain the %s field.", rmd.DecoderName, ROUTER_REQUEST_PATH_FIELD_NAME)
		return nil, err
	}

	// Each pack is decorated by the EventRouter input with a field indicating the request path
	for _, handler := range rmd.requestHandlers {
		matched, err := regexp.MatchString(handler.PathPattern, path.GetValue().(string))
		if nil == err && matched {
			decoder, err = rmd.getDecoder(handler)
			if nil != err {
				return nil, err
			}
			rhConfig = handler
			break
		}
	}
	if nil == decoder {
		err = fmt.Errorf("[%s] No decoder found for path %s.", rmd.DecoderName, path)
		return nil, err
	}

	// add request handler specific fields to pack
	if field, err := message.NewField(ROUTER_HANDLER_PATH_PATTERN_FIELD_NAME, rhConfig.PathPattern, ""); err == nil {
		pack.Message.AddField(field)
	} else {
		err = fmt.Errorf("[Router] can't add field: %s", err)
	}
	if field, err := message.NewField(ROUTER_HANDLER_ACTION_FIELD_NAME, rhConfig.Action, ""); err == nil {
		pack.Message.AddField(field)
	} else {
		err = fmt.Errorf("[Router] can't add field: %s", err)
	}
	if field, err := message.NewField(ROUTER_HANDLER_METHOD_FIELD_NAME, rhConfig.Method, ""); err == nil {
		pack.Message.AddField(field)
	} else {
		err = fmt.Errorf("[Router] can't add field: %s", err)
	}
	if field, err := message.NewField(ROUTER_HANDLER_ENCODER_FIELD_NAME, rhConfig.Encoder, ""); err == nil {
		pack.Message.AddField(field)
	} else {
		err = fmt.Errorf("[Router] can't add field: %s", err)
	}
	if field, err := message.NewField(ROUTER_HANDLER_ANNOTATIONS_FIELD_NAME, rhConfig.Annotations, ""); err == nil {
		pack.Message.AddField(field)
	} else {
		err = fmt.Errorf("[Router] can't add field: %s", err)
	}

	packs, err = decoder.Decode(pack)

	if nil == packs {
		err = fmt.Errorf("[%s] Decoder failed to decode package.", rmd.DecoderName)
		return nil, err
	}

	return packs, nil
}

func (rmd *RouterMultiDecoder) getDecoder(handler RouterRequestHandlerConfig) (decoder pipeline.Decoder, err error) {
	if "" == handler.Decoder {
		err = fmt.Errorf("[%s] No decoder specified for handler", rmd.DecoderName)
		return
	}
	if nil != rmd.Decoders[handler.Decoder] {
		decoder = rmd.Decoders[handler.Decoder]
		return
	}

	var ok bool
	if decoder, ok = rmd.pConfig.Decoder(handler.Decoder); !ok {
		err = fmt.Errorf("[%s] Non-existent subdecoder: %s", rmd.DecoderName, handler.Decoder)
		return
	}
	rmd.Decoders[handler.Decoder] = decoder

	if wanter, ok := decoder.(pipeline.WantsDecoderRunner); ok {
		// pass the outer DecoderRunner to subdecoders
		wanter.SetDecoderRunner(rmd.dRunner)
	}

	return
}

// Heka will call this at DecoderRunner shutdown time, we might need to pass
// this along to subdecoders.
func (rmd *RouterMultiDecoder) Shutdown() {
	for _, decoder := range rmd.Decoders {
		if nil == decoder {
			continue
		}
		if wanter, ok := decoder.(pipeline.WantsDecoderRunnerShutdown); ok {
			wanter.Shutdown()
		}
	}
}

func init() {
	pipeline.RegisterPlugin("RouterMultiDecoder", func() interface{} {
		return new(RouterMultiDecoder)
	})
}
