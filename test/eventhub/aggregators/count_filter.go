package aggregators

import (
	"bitbucket.org/georgiana_m/specs-monitoring/core"
	"encoding/json"
	"fmt"
	"github.com/mozilla-services/heka/pipeline"
	"strings"
	"time"
)

const (
	allComponentsConst       = "all"
	filterHekaMssgTypeConst  = "specs.counter.v1"
	filterComponentNameConst = "specs-counter-filter"
	filterMssgTypeConst      = "specs.counter.v1"
)

// Filter that counts the number of messages flowing through and provides
// primitive aggregation counts.
type SpecsCounterFilter struct {
	lastTime        map[string]time.Time
	lastCount       map[string]uint
	count           map[string]uint
	groupMessagesBy string
}

// SpecsCounterFilter config struct, used only for defining specifying default
// message matcher, count interval values and messaging grouping.
type SpecsCounterFilterConfig struct {
	// Defaults to counting everything except the counter's own output
	// messages.
	MessageMatcher string `toml:"message_matcher"`
	// Defaults to 5 minutes
	CountInterval uint `toml:"ticker_interval"`
	// Defaults to ""
	GroupMessagesBy string `toml:"group_messages_by"`
}

// SpecsCountFilterOutput config struct defines the type of the data that will fill
// the "data" field of the SPECS internal message generated by the filter.
type SpecsCountFilterOutput struct {
	// Specifies the operator used as aggregaton function.
	Operator string `json:"operator"`
	// Specifies the result of the aggregation.
	Result interface{} `json:"result"`
}

// ConfigStruct returns a default-value-populated configuration structure into which
// the plugin's TOML configuration will be deserialized. For more details,
// see https://godoc.org/github.com/mozilla-services/heka/pipeline#HasConfigStruct
func (filter *SpecsCounterFilter) ConfigStruct() interface{} {
	matcher := fmt.Sprintf("Type != `%s`", filterHekaMssgTypeConst)
	return &SpecsCounterFilterConfig{
		MessageMatcher:  matcher,
		CountInterval:   300,
		GroupMessagesBy: "",
	}
}

// Init initializes the filter plugin. For more details,
// see https://godoc.org/github.com/mozilla-services/heka/pipeline#Plugin
func (filter *SpecsCounterFilter) Init(config interface{}) error {
	conf := config.(*SpecsCounterFilterConfig)
	filter.groupMessagesBy = conf.GroupMessagesBy

	filter.count = make(map[string]uint)
	filter.lastCount = make(map[string]uint)
	filter.lastTime = make(map[string]time.Time)

	return nil
}

// Run starts and runs the filter. For more details,
// see https://godoc.org/github.com/mozilla-services/heka/pipeline#Filter
func (filter *SpecsCounterFilter) Run(fr pipeline.FilterRunner, h pipeline.PluginHelper) (err error) {
	inChan := fr.InChan()
	ticker := fr.Ticker()

	var (
		ok           = true
		pack         *pipeline.PipelinePack
		msgLoopCount uint
	)
	for ok {
		select {
		case pack, ok = <-inChan:
			if !ok {
				break
			}
			if filter.groupMessagesBy != "" {
				theField := strings.ToLower(filter.groupMessagesBy)
				// check if it contains the group by field
				mssgField, hasField := pack.Message.GetFieldValue(theField)
				if !hasField {
					pack.Recycle()
					continue
				}
				groupField := mssgField.(string)
				filter.count[groupField]++
			} else {
				filter.count[allComponentsConst]++
			}

			msgLoopCount = pack.MsgLoopCount
			pack.Recycle()
		case <-ticker:
			filter.tally(fr, h, msgLoopCount)
		}

	}
	return
}

// tally is invoked on every tick and computes the result of the aggregation for the
// messages received since the previous tick.
func (filter *SpecsCounterFilter) tally(fr pipeline.FilterRunner, h pipeline.PluginHelper,
	msgLoopCount uint) {
	now := time.Now()
	for key, _ := range filter.count {
		msgsSent := filter.count[key] - filter.lastCount[key]
		if msgsSent == 0 {
			return
		}

		filter.lastCount[key] = filter.count[key]
		filter.lastTime[key] = now
		pack := h.PipelinePack(msgLoopCount)
		if pack == nil {
			fr.LogError(fmt.Errorf("Exceeded MaxMsgLoops = %d",
				h.PipelineConfig().Globals.MaxMsgLoops))
			return
		}
		pack.Message.SetType(filterHekaMssgTypeConst)
		mssg := core.SpecsMonitoringMessage{}
		mssg.Component = filterComponentNameConst
		mssg.Object = "null"
		mssg.Timestamp = float64(now.Unix())
		mssg.Labels = make([]string, 1)
		mssg.Labels[0] = fmt.Sprintf("%s", key)
		mssg.Data = SpecsCountFilterOutput{"count", filter.count[key]}
		mssg.Type = filterMssgTypeConst

		b, err := json.Marshal(mssg)
		if err != nil {
			fr.LogError(fmt.Errorf("Could not serialiaze JSON %+v. Error: %s", mssg, err))
			return
		}
		pack.Message.SetPayload(fmt.Sprintf("%s", b))
		core.AddFields(pack, &mssg)

		fr.Inject(pack)

		filter.count[key] = 0
		//fr.LogMessage(fmt.Sprintf("Sent message %s\n", b))
	}
}

func init() {
	pipeline.RegisterPlugin("SpecsCounterFilter", func() interface{} {
		return new(SpecsCounterFilter)
	})
}
