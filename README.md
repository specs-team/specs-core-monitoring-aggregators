#SPECS monitoring: aggregators

This repository contains some SPECS aggregators to add at Event Hub module for manage the report/alert message. The result will be saved on a test.db file. This is a work in progress. The currently implemented components are described below.


##Table Of Contents
	
1. [Configuring the Event Hub]

2. [Start the aggregators] 

3. [Sending messages]
  
4. [Analysis of the results]



#Configuring the Event Hub

The general architecture of the Event Hub can be found at "test/eventhub" path.
The Event Hub configuration file specifies how to handle received messages and requests for event notifications, and also what Sieve Filters have to load. 
The configuration file is "myfilter.toml", it can be found in a "toml" folder and must be copied at "test/eventhub/" path. 
To do this you can easly start the script "copymyfilter.sh" available in "test/script/" folder, so you have to launch it in the same path.


#Start the aggregators

The use of aggregators extends the standard Event Hub's global configuration.
You can find the aggregator in "aggregators" folder.

In order to start the "aggregators" you have to launch the script "eventhub.sh" that runs Event Hub and aggregators as daemons. You can find this script in "test/script/" folder. As before you have to execute it in the same path: "test/script/".

This script acts as following:

1. runs the Event Hub with the new configuration file;
2. runs the aggregators:
	a. node reportgenerator.js
	b. node deltatimestamp.js
	c. node savedb.js


#Sending messages

In order to send a message to Event Hub, a client should send a HTTP POST request with a path matching one of the Hub's input handlers path pattern. The request should contain a SPECS input message conforming to the JSON format.
You can find a script that generate some properly message and send it to the Event Hub. 
You can find this script "commandlist.sh" in the script folder at "test/script/". As before you must execute it at same path: "test/script/".

For a fast test you can find a script all in one "allinone.sh" in the folder "test/script/" that include "copymyfilter.sh","eventhub.sh" and "commandlist.sh".


#Analysis of the results

In order to see the results we must check the "test.db" file, it will be saved into "test/result" path. In the same path you'll find also those output files :

hekaoutput.txt 
reportgeneratoroutput.txt 
deltatimestampoutput.txt
savedboutput.txt

They are the outputs provided by the Event Hub and Aggregators. They haven't a great importance but for now we decided to keep them for the tests, in the future will be deleted.
