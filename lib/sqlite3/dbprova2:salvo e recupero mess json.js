var jsonReport={
    "component":"openvas",
    "object":"os",
    "labels":["openvas-vulnerability-report"],
    "type":"report",
    "data":{"generatedAlerts":"report number",},
    "timestamp":1414701485,
}

var stringReport= JSON.stringify(jsonReport);
//controllo se il file esiste..
var fs = require("fs");
var file = "test.db";
var exists = fs.existsSync(file);

//..se non esiste lo creo
if(!exists) {
  console.log("Creating DB file.");
  fs.openSync(file, "w");
}

//creo una tabella
var sqlite3 = require("sqlite3").verbose();
var db = new sqlite3.Database(file);

db.serialize(function() {
  if(!exists) {
    db.run("CREATE TABLE Stuff (thing TEXT)");
  }
  
        var stmt = db.prepare("INSERT INTO Stuff VALUES (?)");
  
//inserisco i dati
  var rnd;
  for (var i = 0; i < 10; i++) {
    rnd = Math.floor(Math.random() * 10000000);
    stmt.run("Thing #" + stringReport);
  }
	stmt.finalize();
}); 
/*query
stmt.finalize();
  db.each("SELECT rowid AS id, thing FROM Stuff", function(err, row) {
    console.log(row.id + ": " + row.thing);
  });
});*/
//chiudo il db
db.close();
