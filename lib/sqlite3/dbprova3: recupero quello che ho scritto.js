var file = "test.db";
var sqlite3 = require("sqlite3").verbose();
var db = new sqlite3.Database(file);

db.each("SELECT rowid AS id, thing FROM Stuff", function(err, row) {
    console.log(row.id + ": " + row.thing);
  });

db.close();
