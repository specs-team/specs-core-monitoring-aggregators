//variabili per il database
var fs = require("fs");
var file = "../test/result/test.db";
var exists = fs.existsSync(file);
if(!exists) {
  console.log("Creating DB file.");
  fs.openSync(file, "w");
}
var sqlite3 = require("../lib/sqlite3").verbose();
var db = new sqlite3.Database(file);
//inizializzo il database
db.serialize(function() {
  if(!exists) {
    db.run("CREATE TABLE Metrics ( Sla_id TEXT, Security_mechanism TEXT, Security_Metric TEXT,Value TEXT,Timestamp TEXT )");
  }
});
///////////////////////////

var http = require('http');

var ConsumeAddress = {
  host: 'localhost',
  port:'8080',
  path: '/streams/securitymetric/consume',
  method: 'GET'
};

var reportCallback = function(event) {	
	console.log('Report Sent to Eventhub and saved on database..');
}

var consumeCallback= function(res) {
  console.log('STATUS: ' + res.statusCode);
  console.log('\nHEADERS: ' + JSON.stringify(res.headers));
  res.on('data', function (chunk) {
  var event=JSON.parse(chunk);
  
  
				
				//salvo nel database
				var stmt = db.prepare("INSERT INTO metrics VALUES (?,?,?,?,?)");
				var slaid= event.labels[0].replace(/sla_id_/i, '');
				var mechanism= event.labels[1].replace(/security_mechanism_/i, '');

				stmt.run(slaid,mechanism,"specs.eu/metric/"+event.labels[3],event.data,event.timestamp);
				stmt.finalize();
				
			
  
  });
};
 
var consumeRequest = http.request(ConsumeAddress, consumeCallback);
  consumeRequest.on('error', function(e) {
  console.log('problem with request: ' + e.message);
  });
consumeRequest.end();



