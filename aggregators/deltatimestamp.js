var http = require('http');

var ConsumeAddress = {
  host: 'localhost',
  port:'8080',
  path: '/streams/report/consume',
  method: 'GET'
};

var ProduceAddress = {
  host: 'localhost',
  port:'8080',
  path: '/events/input',
  method: 'POST'
};

var cont=0;
var componentCounterIDs =[];
var componentCounterValues =[];
var componentCounterTimestamp =[];

var reportCallback = function(event) {	
	console.log('Report with label secutiry_metric sent..');
}

var consumeCallback= function(res) {
  console.log('STATUS: ' + res.statusCode);
  console.log('\nHEADERS: ' + JSON.stringify(res.headers));
  res.on('data', function (chunk) {
  var event=JSON.parse(chunk);
  //
  cont++;
  console.log('\nMessagge '+cont+': '+event.labels[1]+' alert:' + chunk);
  if( componentCounterIDs.length == 0 ){
	componentCounterIDs[0]=event.labels[1];
	componentCounterValues[0]=1;
	componentCounterTimestamp[0]=event.timestamp;
	console.log('Conteggio '+event.labels[1]+':'+componentCounterValues[0]);
  } else {
	var test=0;
	for ( index=0; index<componentCounterIDs.length; index++){
		if( componentCounterIDs[index] == event.labels[1] ) {
			componentCounterValues[index]=componentCounterValues[index]+1;
			test=1;
			console.log('Conteggio '+event.labels[1]+':'+componentCounterValues[index]);			
					var timeReport=(event.timestamp - componentCounterTimestamp[index]);
					componentCounterTimestamp[index]=event.timestamp;
					console.log('Time from last report '+event.labels[1]+': '+timeReport);
					var jsonReportossec={
   					"component":event.component,
    					"object":event.object,
    					"labels":[event.labels[0],event.labels[1],event.labels[2],"M16_dos_report_max_age"],
    					"type":event.type,
    					"data":timeReport,
    					"timestamp":event.timestamp
  					}
					var stringReportossec=JSON.stringify(jsonReportossec);					
					var jsonReportopenvas={
   					"component":event.component,
    					"object":event.object,
    					"labels":[event.labels[0],event.labels[1],event.labels[2],"M14_dos_report_max_age"],
    					"type":event.type,
    					"data":timeReport,
    					"timestamp":event.timestamp
  					}
  					var stringReportopenvas= JSON.stringify(jsonReportopenvas);
					var jsonReportopenvasVU={
   					"component":event.component,
    					"object":event.object,
    					"labels":[event.labels[0],event.labels[1],event.labels[2],"M13_vulnerability_report_max_age"],
    					"type":event.type,
    					"data":timeReport,
    					"timestamp":event.timestamp
  					}
  					var stringReportopenvasVU= JSON.stringify(jsonReportopenvasVU);
					var reportReqest=http.request(ProduceAddress,reportCallback(event.labels[1]));
					if(componentCounterIDs[index]=='security_mechanism_specs-monitoring-ossec'){
						reportReqest.write(stringReportossec);
						reportReqest.end();
					}if(componentCounterIDs[index]=='security_mechanism_specs-monitoring-openvas'){
						if(event.object=='vulnerability_list'){						
						reportReqest.write(stringReportopenvasVU);
						reportReqest.end();
						}else{
						reportReqest.write(stringReportopenvas);
						reportReqest.end();
						}				
					}
		} 
	}
        if( test == 0 ) {
		componentCounterIDs[componentCounterIDs.length]=event.labels[1];
		componentCounterValues[componentCounterValues.length]=1;
		componentCounterTimestamp[componentCounterTimestamp.length]=event.timestamp;
		console.log('Conteggio '+event.labels[1]+':'+componentCounterValues[componentCounterValues.length-1]);
	}
  }
  
  });
};
 
var consumeRequest = http.request(ConsumeAddress, consumeCallback);
  consumeRequest.on('error', function(e) {
  console.log('problem with request: ' + e.message);
  });
consumeRequest.end();



