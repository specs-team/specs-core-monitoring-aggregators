var http = require('http');

var ConsumeAddress = {
  host: 'localhost',
  port:'8080',
  path: '/streams/alert/consume',
  method: 'GET'
};

var ProduceAddress = {
  host: 'localhost',
  port:'8080',
  path: '/events/input',
  method: 'POST'
};

var cont=0;
var componentCounterIDs =[];
var componentCounterValues=[];

var reportCallback = function(event) {	
	console.log('Report Sent..');
}

var consumeCallback= function(res) {
  console.log('STATUS: ' + res.statusCode);
  console.log('\nHEADERS: ' + JSON.stringify(res.headers));
  res.on('data', function (chunk) {
  var event=JSON.parse(chunk);
  var jsonReport={
    "component":event.component,
    "object":event.os,
    "labels":[event.labels[0],event.labels[1],"report"],
    "type":event.type,
    "data":event.data,
    "timestamp":event.timestamp
  }
  var stringReport= JSON.stringify(jsonReport);
  cont++;
  console.log('\nMessagge '+cont+': '+event.labels[1]+' alert:' + chunk);
  if( componentCounterIDs.length == 0 ){
	componentCounterIDs[0]=event.labels[1];
	componentCounterValues[0]=1;
	console.log('Conteggio '+event.labels[1]+':'+componentCounterValues[0]);
  } else {
	var test=0;
	for ( index=0; index<componentCounterIDs.length; index++){
		if( componentCounterIDs[index] == event.labels[1] ) {
			componentCounterValues[index]=componentCounterValues[index]+1;
			test=1;
			console.log('Conteggio '+event.labels[1]+':'+componentCounterValues[index]);
			if( (componentCounterValues[index] % 5) == 0 ){
			if(componentCounterIDs[index]!='security_mechanism_specs-monitoring-openvas'){
				var reportReqest=http.request(ProduceAddress,reportCallback(event.labels[1]));
				reportReqest.write(stringReport);
				reportReqest.end();
			}
			}	
		} 
	}
        if( test == 0 ) {
		componentCounterIDs[componentCounterIDs.length]=event.labels[1];
		componentCounterValues[componentCounterValues.length]=1;
		console.log('Conteggio '+event.labels[1]+':'+componentCounterValues[componentCounterValues.length-1]);
	}
  }
  
  });
};
 
var consumeRequest = http.request(ConsumeAddress, consumeCallback);
  consumeRequest.on('error', function(e) {
  console.log('problem with request: ' + e.message);
  });
consumeRequest.end();



